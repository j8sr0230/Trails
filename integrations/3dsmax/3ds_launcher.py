"""
Copyright (C) 2015-2019  ?? Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import ptvsd
import pymxs
from trails.app import Trails
from PySide2 import QtWidgets
from PySide2 import QtCore

ptvsd.enable_attach(address=('0.0.0.0', 3000), redirect_output=True)

mainWindow = QtWidgets.QWidget.find(pymxs.runtime.windows.getMAXHWND())

if Trails.appInstance is None:
    instance = Trails.instance(mainWindow, "3dsmax")
    instance.show()
