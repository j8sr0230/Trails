"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.tests.TestsBase import *
from trails.core.common import connectPins, DEFAULT_IN_EXEC_NAME, DEFAULT_OUT_EXEC_NAME


class TestBasePackage(unittest.TestCase):

    def setUp(self):
        print('\t[BEGIN TEST]', self._testMethodName)

    def tearDown(self):
        print('--------------------------------\n')

    def test_branch_node(self):
        packages = GET_PACKAGES()
        man = GraphManager()
        foos = packages['TrailsBase'].GetFunctionLibraries()["DefaultLib"].getFunctions()
        nodes = packages['TrailsBase'].GetNodeClasses()
        printNode1 = nodes["consoleOutput"]("print")
        man.activeGraph().addNode(printNode1)
        printNode1.setData(str('entity'), "first")

        branchNode = nodes["branch"]("branchNODE")
        self.assertIsNotNone(branchNode, "branch node is not created")
        man.activeGraph().addNode(branchNode)
        branchNode.setData('Condition', True)

        connected = connectPins(printNode1[str(DEFAULT_OUT_EXEC_NAME)], branchNode[str("In")])
        self.assertEqual(connected, True, "failed to connect")

        printNodeTrue = nodes["consoleOutput"]("print")
        man.activeGraph().addNode(printNodeTrue)
        printNodeTrue.setData('entity', "True executed")

        printNodeFalse = nodes["consoleOutput"]("print")
        man.activeGraph().addNode(printNodeFalse)
        printNodeFalse.setData('entity', "False executed")

        connectPins(branchNode[str('True')], printNodeTrue[DEFAULT_IN_EXEC_NAME])
        connectPins(branchNode[str('False')], printNodeFalse[DEFAULT_IN_EXEC_NAME])

        printNode1.call(DEFAULT_IN_EXEC_NAME, message="TEST MESSAGE")
