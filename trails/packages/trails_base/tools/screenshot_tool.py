"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.ui.tool.tool import ShelfTool
from trails.packages.trails_base.tools import RESOURCES_DIR
from trails.ui.context_menu_data_builder import ContextMenuDataBuilder

from Qt import QtGui
from Qt.QtWidgets import QFileDialog


class ScreenshotTool(ShelfTool):
    """docstring for ScreenshotTool."""
    def __init__(self):
        super(ScreenshotTool, self).__init__()
        self.format = "PNG"

    def getState(self):
        return dict(
            **super().getState(),
            format=self.format)

    def restoreState(self, settings):
        super(ScreenshotTool, self).restoreState(settings)
        formatValue = settings.format
        if formatValue is not None:
            self.format = formatValue
        else:
            self.format = "PNG"

    def onSetFormat(self, fmt):
        self.format = fmt

    def contextMenuBuilder(self):
        builder = ContextMenuDataBuilder()
        builder.addEntry("Save to PNG", "PNG", lambda: self.onSetFormat("PNG"))
        builder.addEntry("Save to JPG", "JPG", lambda: self.onSetFormat("JPG"))
        return builder

    @staticmethod
    def toolTip():
        return "Takes screenshot of visible area of canvas and\nsaves image to file"

    @staticmethod
    def getIcon():
        return QtGui.QIcon(RESOURCES_DIR + "screenshot_icon.png")

    @staticmethod
    def name():
        return str("ScreenshotTool")

    def do(self):
        name_filter = "Image (*.{0})".format(self.format.lower())
        fName = QFileDialog.getSaveFileName(filter=name_filter)
        if not fName[0] == '':
            print("save screen to {0}".format(fName[0]))
            img = self.trailsInstance.getCanvas().grab()
            img.save(fName[0], format=self.format, quality=100)
