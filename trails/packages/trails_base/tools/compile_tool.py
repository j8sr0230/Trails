"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.ui.tool.tool import ShelfTool
from trails.packages.trails_base.tools import RESOURCES_DIR
from trails.ui.context_menu_data_builder import ContextMenuDataBuilder

from Qt import QtGui


class CompileTool(ShelfTool):
    """docstring for CompileTool."""
    def __init__(self):
        super(CompileTool, self).__init__()

    def onSetFormat(self, fmt):
        self.format = fmt

    @staticmethod
    def toolTip():
        return "Ensures everything is ok!"

    @staticmethod
    def getIcon():
        return QtGui.QIcon(RESOURCES_DIR + "compile.png")

    @staticmethod
    def name():
        return str("CompileTool")

    def do(self):
        for node in self.trailsInstance.graphManager.get().getAllNodes():
            node.checkForErrors()
