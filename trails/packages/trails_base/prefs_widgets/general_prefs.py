"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import os

from Qt import QtCore
from Qt.QtWidgets import *
from Qt.QtGui import *

from trails.config_manager import PreferencesManager
from trails.ui.editor_history import EditorHistory
from trails.ui.widgets.properties_framework import CollapsibleFormWidget
from trails.ui.widgets.preferences_window import CategoryWidgetBase


NEW_PATH = "add new path…"


def cleanPath(path):
    """Clean a path.

    The path is cleaned:
        - by normalizing it, and
        - byreplacing the user's directory with a tilde.

    Parameters
    ----------
    path : str
        A string describing the path.

    Returns
    -------
    path : str
        The cleaned path.

    """
    userdir = os.path.expanduser('~')

    path = os.path.normpath(path)
    if path.startswith(userdir):
        path = path.replace(userdir, "~")
    return path


class PackagePathTreeItem(QTreeWidgetItem):
    """QTreeWidgetItem showing a path, a delete and a select directory button."""

    def __init__(self, path, parent, model):
        """Initializes the `PackagePathTreeItem` class.

        Parameters
        ----------
        path : str|None
            The path to add.
            If `None` an "Add a new directory" row is added.
        parent : QTreeWidget
            The QTreeWidget, where the row should show.
        model : list
            The list with the already added paths.

        """
        # Olean the path, and exit, if the path is already in the model
        if path is not None:
            path = cleanPath(path)
            if path in model:
                QMessageBox.information(
                    None,
                    "Add additional package location",
                    f"Selected path '{path}' is already in the list")
                return

        super().__init__()
        self.parent = parent
        self.model = model
        self.path = path

        # Set text of first column (or a disabled label for adding a new directory)
        if path is None:
            label = QLabel(NEW_PATH)
            label.setEnabled(False)
        else:
            self.setText(0, path)
            self.setToolTip(0, path)

        # Make a delete button for the second column
        if path is None:
            # Do not add a delete button
            pass
        else:
            # Add delete button
            self.delButton = QToolButton()
            self.delButton.setToolTip("Delete directory")
            self.delButton.setIcon(QIcon(":/delete_icon.png"))
            self.delButton.clicked.connect(self.onDelButton)

        # Make a directory select button for the third column
        self.dirButton = QToolButton()
        self.dirButton.setToolTip("Add new directory" if path is None else "Change directory")
        self.dirButton.setIcon(QIcon(":/folder_open_icon.png"))
        if path is None:
            self.dirButton.clicked.connect(self.onAddDirButton)
        else:
            self.dirButton.clicked.connect(self.onDirButton)

        # Append row and path
        if path is None:
            parent.addTopLevelItem(self)
        else:
            # Note, we should have the "add new path" item in the last position!
            parent.insertTopLevelItem(parent.topLevelItemCount() - 1, self)
            model.append(path)

        # Add widgets to the columns
        if path is None:
            parent.setItemWidget(self, 0, label)
        else:
            parent.setItemWidget(self, 1, self.delButton)
        parent.setItemWidget(self, 2, self.dirButton)

    def onDelButton(self):
        """Callback function to delete a path entry."""
        path = self.path
        idx = self.model.index(path)
        self.model.remove(path)
        self.parent.takeTopLevelItem(idx)
        del self

    def onAddDirButton(self):
        """Callback function to add a new path entry."""
        # Get new path
        newDir = QFileDialog.getExistingDirectory(
            None,
            'Add extra package directory',
            '',
            QFileDialog.ShowDirsOnly)

        if newDir:
            # Add a new path
            newDir = cleanPath(newDir)
            PackagePathTreeItem(newDir, self.parent, self.model)

    def onDirButton(self):
        """Callback function to change a path entry."""
        # Get new path
        newDir = QFileDialog.getExistingDirectory(
            None,
            'Change extra package directory',
            self.path,
            QFileDialog.ShowDirsOnly)

        if newDir:
            newDir = cleanPath(newDir)
            if newDir != self.path:
                # Change path
                if newDir in self.model:
                    # Selected directory is already in list, do not change it
                    QMessageBox.information(
                        None,
                        "Add additional package location",
                        f"Selected path '{newDir}' is already in the list\n"
                        f"Path was not changed!")
                else:
                    # Replace the old with the new path
                    self.setText(0, newDir)
                    self.setToolTip(0, newDir)
                    self.model[self.model.index(self.path)] = newDir
                    self.path = newDir


class GeneralPreferences(CategoryWidgetBase):
    """docstring for GeneralPreferences."""

    manager = PreferencesManager()
    section = "General"

    def __init__(self, parent=None):
        super(GeneralPreferences, self).__init__(parent)
        settings = self.manager[self.section]

        self.layout = QVBoxLayout(self)
        self.layout.setContentsMargins(1, 1, 1, 1)
        self.layout.setSpacing(2)

        commonCategory = CollapsibleFormWidget(headName="Common")

        self.extraPackageDirs = []
        for path in settings.ExtraPackageDirs:
            path = cleanPath(path)
            if path not in self.extraPackageDirs:
                self.extraPackageDirs.append(path)

        self.additionalPackagePaths = QTreeWidget()
        self.additionalPackagePaths.setIndentation(0)
        self.additionalPackagePaths.setColumnCount(3)
        self.additionalPackagePaths.setHeaderLabels(("Path", "Del", "Set"))
        self.additionalPackagePaths.resizeColumnToContents(1)
        self.additionalPackagePaths.resizeColumnToContents(2)
        commonCategory.addWidget("Additional package locations\n(needs restart)", self.additionalPackagePaths)
        self.layout.addWidget(commonCategory)

        self.lePythonEditor = QLineEdit(settings.EditorCmd)
        commonCategory.addWidget("External text editor", self.lePythonEditor)

        self.historyDepth = QSpinBox()
        self.historyDepth.setRange(10, 100)

        def setHistoryCapacity(val):
            EditorHistory().capacity = val
        self.historyDepth.valueChanged.connect(setHistoryCapacity)
        commonCategory.addWidget("History depth", self.historyDepth)

        self.autoZoom = QCheckBox(self)
        self.autoZoom.stateChanged.connect(self.setAutoZoom)
        commonCategory.addWidget("Automatic zoom", self.autoZoom)

        self.adjAutoZoom = QDoubleSpinBox()
        self.adjAutoZoom.setMinimum(0)
        self.adjAutoZoom.setSingleStep(0.1)
        self.adjAutoZoom.valueChanged.connect(self.setZoom)
        commonCategory.addWidget("Adjust automatic zoom", self.adjAutoZoom)

        self.initialZoom = QDoubleSpinBox()
        self.initialZoom.setMinimum(0)
        self.initialZoom.setSingleStep(0.1)
        self.initialZoom.valueChanged.connect(self.setZoom)
        commonCategory.addWidget("Initial zoom factor", self.initialZoom)

        self.redirectOutput = QCheckBox(self)
        commonCategory.addWidget("Redirect output", self.redirectOutput)

        spacerItem = QSpacerItem(10, 10, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.layout.addItem(spacerItem)

    def setAutoZoom(self, val):
        self.adjAutoZoom.setEnabled(val == QtCore.Qt.Checked)
        self.initialZoom.setEnabled(val != QtCore.Qt.Checked)
        self.setZoom()

    def setZoom(self):
        trails = self.parent().parent().parent().parent().parent()
        if self.autoZoom.checkState() == QtCore.Qt.Checked:
            # Auto zoom
            zoom = self.adjAutoZoom.value()
            trails.canvasWidget.canvas.setAutoZoom(zoom)
        else:
            # Manual zoom
            zoom = self.initialZoom.value()
            trails.canvasWidget.canvas.setZoom(zoom)

    def serialize(self):
        return {
            "EditorCmd": self.lePythonEditor.text(),
            "ExtraPackageDirs": self.extraPackageDirs,
            "HistoryDepth": self.historyDepth.value(),
            "AutoZoom": self.autoZoom.checkState() == QtCore.Qt.Checked,
            "AdjAutoZoom": self.adjAutoZoom.value(),
            "InitialZoom": self.initialZoom.value(),
            "RedirectOutput": self.redirectOutput.checkState() == QtCore.Qt.Checked
            }

    def onShow(self):
        settings = self.manager[self.section]

        self.lePythonEditor.setText(settings.EditorCmd)

        self.additionalPackagePaths.clear()
        self.extraPackageDirs = []
        # Row to add new path
        PackagePathTreeItem(None, self.additionalPackagePaths, self.extraPackageDirs)
        for path in settings.ExtraPackageDirs:
            PackagePathTreeItem(path, self.additionalPackagePaths, self.extraPackageDirs)

        self.historyDepth.setValue(settings.HistoryDepth)

        self.autoZoom.setChecked(settings.AutoZoom)
        self.adjAutoZoom.setValue(settings.AdjAutoZoom)
        self.initialZoom.setValue(settings.InitialZoom)

        self.redirectOutput.setChecked(settings.RedirectOutput)
