"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import math
import random

from trails.core import(
    FunctionLibraryBase,
    IMPLEMENT_NODE
)
from trails.core.common import PinSpecifiers, NodeMeta, REF


class MathLib(FunctionLibraryBase):
    """
    Python builtin math module wrapper
    """

    def __init__(self, packageName):
        super(MathLib, self).__init__(packageName)

    # ###################
    # builtin python math
    # ###################
    @staticmethod
    @IMPLEMENT_NODE(returns=("AnyPin", 0, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.SUPPORTED_DATA_TYPES: ["FloatPin", "IntPin"]}), meta={NodeMeta.CATEGORY: 'Python|math|Number-theoretic and representation functions', NodeMeta.KEYWORDS: []})
    def copysign(x=("AnyPin", 0, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.SUPPORTED_DATA_TYPES: ["FloatPin", "IntPin"]}), y=("AnyPin", 0, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.SUPPORTED_DATA_TYPES: ["FloatPin", "IntPin"]})):
        '''Return <var>x</var> with the sign of <var>y</var>. On a platform that supports signed zeros, <var>copysign(1.0, -0.0)</var> returns <var>-1.0</var>.'''
        return math.copysign(x, y)

    @staticmethod
    @IMPLEMENT_NODE(returns=("AnyPin", 0, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.SUPPORTED_DATA_TYPES: ["FloatPin", "IntPin"]}), meta={NodeMeta.CATEGORY: 'Python|math|Number-theoretic and representation functions', NodeMeta.KEYWORDS: []})
    def fmod(x=("AnyPin", 0, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.SUPPORTED_DATA_TYPES: ["FloatPin", "IntPin"]}), y=("AnyPin", 0, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.SUPPORTED_DATA_TYPES: ["FloatPin", "IntPin"]})):
        '''Return <var>fmod(x, y)</var>, as defined by the platform C library.'''
        return math.fmod(x, y)

    @staticmethod
    @IMPLEMENT_NODE(returns=None, meta={NodeMeta.CATEGORY: 'Python|math|Number-theoretic and representation functions', NodeMeta.KEYWORDS: []})
    def modf(x=("FloatPin", 0.0), f=(REF, ('FloatPin', 0.0)), i=(REF, ('FloatPin', 0.0))):
        '''Return the fractional and integer parts of <var>x</var>. Both results carry the sign of <var>x</var> and are floats.'''
        t = math.modf(x)
        f(t[0])
        i(t[1])

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Number-theoretic and representation functions', NodeMeta.KEYWORDS: []})
    def ceil(x=('FloatPin', 0.0)):
        '''Return the ceiling of <var>x</var> as a float, the smallest integer value greater than or equal to <var>x</var>.'''
        return math.ceil(x)

    @staticmethod
    @IMPLEMENT_NODE(returns=('IntPin', 0), meta={NodeMeta.CATEGORY: 'Python|math|Number-theoretic and representation functions', NodeMeta.KEYWORDS: []})
    def factorial(x=('IntPin', 0), result=(REF, ('BoolPin', False))):
        '''Return <var>x</var> factorial. Raises ValueError if <var>x</var> is not integral or is negative.'''
        try:
            f = math.factorial(x)
            result(True)
            return f
        except:
            result(False)
            return -1

    @staticmethod
    @IMPLEMENT_NODE(returns=('IntPin', 0), meta={NodeMeta.CATEGORY: 'Python|math|Number-theoretic and representation functions', NodeMeta.KEYWORDS: []})
    def floor(x=('FloatPin', 0.0)):
        '''Return the floor of x as an Integral.'''
        return math.floor(x)

    @staticmethod
    @IMPLEMENT_NODE(returns=None, meta={NodeMeta.CATEGORY: 'Python|math|Number-theoretic and representation functions', NodeMeta.KEYWORDS: []})
    def frexp(x=('FloatPin', 0.0), m=(REF, ('FloatPin', 0.0)), e=(REF, ('IntPin', 0))):
        '''Return the mantissa and exponent of <var>x</var> as the pair (m, e). m is <var>x</var> float and e is an integer such that <var>x == m · 2<sup>e</sup></var> exactly.'''
        t = math.frexp(x)
        m(t[0])
        e(t[1])

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Number-theoretic and representation functions', NodeMeta.KEYWORDS: []})
    def fsum(arr=('FloatPin', []), result=(REF, ('BoolPin', False))):
        '''Return an accurate floating point sum of values in the iterable. Avoids loss of precision by tracking multiple intermediate partial sums.'''
        try:
            s = math.fsum([i for i in arr])
            result(True)
            return s
        except:
            result(False)
            return 0.0

    @staticmethod
    @IMPLEMENT_NODE(returns=('BoolPin', False), meta={NodeMeta.CATEGORY: 'Python|math|Number-theoretic and representation functions', NodeMeta.KEYWORDS: []})
    def isinf(x=('FloatPin', 0.0)):
        '''Check if the float <var>x</var> is positive or negative infinity.'''
        return math.isinf(x)

    @staticmethod
    @IMPLEMENT_NODE(returns=('BoolPin', False), meta={NodeMeta.CATEGORY: 'Python|math|Number-theoretic and representation functions', NodeMeta.KEYWORDS: []})
    def isnan(x=('FloatPin', 0.0)):
        '''Check if the float <var>x</var> is a NaN (not a number).'''
        return math.isnan(x)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Number-theoretic and representation functions', NodeMeta.KEYWORDS: []})
    def ldexp(x=('FloatPin', 0.0), i=('IntPin', 0)):
        '''Return <var>x · (2<sup>i</sup>)</var>. This is essentially the inverse of function <var>frexp()</var>.'''
        return math.ldexp(x, i)

    @staticmethod
    @IMPLEMENT_NODE(returns=('IntPin', 0), meta={NodeMeta.CATEGORY: 'Python|math|Number-theoretic and representation functions', NodeMeta.KEYWORDS: []})
    def trunc(x=('FloatPin', 0.0)):
        '''Return the Real value <var>x</var> truncated to an Integral (usually a long integer).'''
        return math.trunc(x)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Power and logarithmic functions', NodeMeta.KEYWORDS: []})
    def exp(x=('FloatPin', 0.0)):
        '''Return <var>e<sup>x</sup></var>.'''
        return math.exp(x)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Power and logarithmic functions', NodeMeta.KEYWORDS: []})
    def expm1(x=('FloatPin', 0.1)):
        '''Return <var>e<sup>x</sup> - 1</var>. For small floats <var>x</var>, the subtraction in <var>exp(x) - 1</var> can result in a significant loss of precision.'''
        return math.expm1(x)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Power and logarithmic functions', NodeMeta.KEYWORDS: []})
    def log(x=('FloatPin', 1.0), base=('FloatPin', math.e), result=(REF, ('BoolPin', False))):
        '''Return the logarithm of <var>x</var> to the given base, calculated as <var>log(x)/log(base)</var>.'''
        try:
            result(True)
            return math.log(x, base)
        except:
            result(False)
            return -1

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Power and logarithmic functions', NodeMeta.KEYWORDS: []})
    def log1p(x=('FloatPin', 1.0), result=(REF, ('BoolPin', False))):
        '''Return the natural logarithm of <var>1+x</var> (base e). The result is calculated in a way which is accurate for <var>x</var> near zero.'''
        try:
            result(True)
            return math.log1p(x)
        except:
            result(False)
            return -1

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Power and logarithmic functions', NodeMeta.KEYWORDS: []})
    def log10(x=('FloatPin', 1.0), result=(REF, ('BoolPin', False))):
        '''Return the base-10 logarithm of <var>x</var>. This is usually more accurate than <var>log(x, 10)</var>.'''
        try:
            result(True)
            return math.log10(x)
        except:
            result(False)
            return -1

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Power and logarithmic functions', NodeMeta.KEYWORDS: []})
    def power(x=('FloatPin', 0.0), y=('FloatPin', 0.0), result=(REF, ('BoolPin', False))):
        '''Return <var>x</var> raised to the power <var>y</var>.'''
        try:
            result(True)
            return math.pow(x, y)
        except:
            result(False)
            return -1

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Power and logarithmic functions', NodeMeta.KEYWORDS: []})
    def sqrt(x=('FloatPin', 0.0), result=(REF, ('BoolPin', False))):
        '''Return the square root of <var>x</var>.'''
        try:
            result(True)
            return math.sqrt(x)
        except:
            result(False)
            return -1

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Trigonometry', NodeMeta.KEYWORDS: []})
    def cos(rad=('FloatPin', 0.0)):
        '''Return the cosine of <var>x</var> radians.'''
        return math.cos(rad)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Trigonometry', NodeMeta.KEYWORDS: []})
    def acos(rad=('FloatPin', 0.0)):
        '''Return the arc cosine of <var>x</var>, in radians.'''
        return math.acos(rad)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Trigonometry', NodeMeta.KEYWORDS: []})
    def sin(rad=('FloatPin', 0.0)):
        '''Return the sine of <var>x</var> radians.'''
        return math.sin(rad)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Trigonometry', NodeMeta.KEYWORDS: []})
    def asin(rad=('FloatPin', 0.0)):
        '''Return the arc sine of <var>x</var>, in radians.'''
        return math.asin(rad)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Trigonometry', NodeMeta.KEYWORDS: []})
    def tan(rad=('FloatPin', 0.0)):
        '''Return the tangent of <var>x</var> radians.'''
        return math.tan(rad)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Trigonometry', NodeMeta.KEYWORDS: []})
    def atan(rad=('FloatPin', 0.0)):
        '''Return the arc tangent of <var>x</var>, in radians.'''
        return math.atan(rad)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Trigonometry', NodeMeta.KEYWORDS: []})
    def atan2(x=('FloatPin', 0.0), y=('FloatPin', 0.0)):
        '''Return <var>atan(a / b)</var>, in radians. The result is between <var>-π</var> and <var>π</var>. <br/>The vector in the plane from the origin to point (x, y) makes this angle with the positive X axis. The point of <var>atan2()</var> is that the signs of both inputs are known to it, so it can compute the correct quadrant for the angle. <br/>For example, <var>atan(1)</var> and <var>atan2(1, 1)</var> are both <var>π/4</var>, but <var>atan2(-1, -1)</var> is <var>-3*π/4</var>.'''
        return math.atan2(x, y)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Trigonometry', NodeMeta.KEYWORDS: []})
    def hypot(x=('FloatPin', 0.0), y=('FloatPin', 0.0)):
        '''Return the Euclidean norm, <var>sqrt(x*x + y*y)</var>. This is the length of the vector from the origin to point (x, y).'''
        return math.hypot(x, y)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Angular conversion', NodeMeta.KEYWORDS: []})
    def degtorad(deg=('FloatPin', 0.0)):
        '''Convert angle <var>x</var> from degrees to radians.'''
        return math.radians(deg)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Angular conversion', NodeMeta.KEYWORDS: []})
    def radtodeg(rad=('FloatPin', 0.0)):
        '''Convert angle <var>x</var> from radians to degrees.'''
        return math.degrees(rad)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Hyperbolic functions', NodeMeta.KEYWORDS: []})
    def acosh(x=('FloatPin', 0.0), Result=(REF, ('BoolPin', False))):
        '''Return the inverse hyperbolic cosine of <var>x</var>.'''
        try:
            Result(True)
            return math.acosh(x)
        except:
            Result(False)
            return -1

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Hyperbolic functions', NodeMeta.KEYWORDS: []})
    def asinh(x=('FloatPin', 0.0)):
        '''Return the inverse hyperbolic sine of x.'''
        return math.asinh(x)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Hyperbolic functions', NodeMeta.KEYWORDS: []})
    def atanh(x=('FloatPin', 0.0), Result=(REF, ('BoolPin', False))):
        '''Return the inverse hyperbolic tangent of <var>x</var>.'''
        try:
            Result(True)
            return math.atanh(x)
        except:
            Result(False)
            return -1

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Hyperbolic functions', NodeMeta.KEYWORDS: []})
    def cosh(x=('FloatPin', 0.0), Result=(REF, ('BoolPin', False))):
        '''Return the hyperbolic cosine of <var>x</var>.'''
        try:
            Result(True)
            return math.cosh(x)
        except:
            Result(False)
            return -1

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Hyperbolic functions', NodeMeta.KEYWORDS: []})
    def sinh(x=('FloatPin', 0.0), Result=(REF, ('BoolPin', False))):
        '''Return the hyperbolic sine of <var>x</var>.'''
        try:
            Result(True)
            return math.sinh(x)
        except:
            Result(False)
            return -1

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Hyperbolic functions', NodeMeta.KEYWORDS: []})
    def tanh(x=('FloatPin', 0.0)):
        '''Return the hyperbolic tangent of <var>x</var>.'''
        return math.tanh(x)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Special functions', NodeMeta.KEYWORDS: []})
    def erf(x=('FloatPin', 0.0)):
        '''Return the error function at <var>x</var>.'''
        return math.erf(x)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Special functions', NodeMeta.KEYWORDS: []})
    def erfc(x=('FloatPin', 0.0)):
        '''Return the complementary error function at <var>x</var>.'''
        return math.erfc(x)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Special functions', NodeMeta.KEYWORDS: []})
    def gamma(x=('FloatPin', 0.0), Result=(REF, ('BoolPin', False))):
        '''Return the Gamma function at <var>x</var>.'''
        try:
            Result(True)
            return math.gamma(x)
        except:
            Result(False)
            return -1

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Special functions', NodeMeta.KEYWORDS: []})
    def lgamma(x=('FloatPin', 0.0), Result=(REF, ('BoolPin', False))):
        '''Return the natural logarithm of the absolute value of the Gamma function at <var>x</var>.'''
        try:
            Result(True)
            return math.lgamma(x)
        except:
            Result(False)
            return -1

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Constants', NodeMeta.KEYWORDS: []})
    def e():
        '''The mathematical constant <var>e = 2.718281</var>, to available precision.'''
        return math.e

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Python|math|Constants', NodeMeta.KEYWORDS: []})
    def pi():
        '''The mathematical constant <var>π = 3.141592</var>, to available precision.'''
        return math.pi
