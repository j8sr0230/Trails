"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import os
import platform
from copy import copy, deepcopy
from trails.core import(
    FunctionLibraryBase,
    IMPLEMENT_NODE
)
from trails import getHashableDataTypes
from trails.core.common import PinOptions, PinSpecifiers, NodeTypes, NodeMeta, currentProcessorTime, REF
from trails.core.paths_registry import PathsRegistry


PIN_ALLOWS_ANYTHING = {PinSpecifiers.ENABLED_OPTIONS: PinOptions.AllowAny | PinOptions.ArraySupported | PinOptions.DictSupported}


class DefaultLib(FunctionLibraryBase):
    """Default library builting stuff, variable types and conversions
    """

    def __init__(self, packageName):
        super(DefaultLib, self).__init__(packageName)

    @staticmethod
    @IMPLEMENT_NODE(returns=('AnyPin', None, {PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny | PinOptions.DictElementSupported, PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.STRUCT_CONSTRAINT: "1"}),
                    meta={NodeMeta.CATEGORY: 'Utils', NodeMeta.KEYWORDS: ['id']})
    def copyObject(obj=('AnyPin', None, {PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny | PinOptions.DictElementSupported, PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.STRUCT_CONSTRAINT: "1"}), deepCopy=("BoolPin", False)):
        '''Shallow or deep copy of an object.'''
        copyFunction = deepcopy if deepCopy else copy
        return copyFunction(obj)

    @staticmethod
    @IMPLEMENT_NODE(returns=None, nodeType=NodeTypes.Callable, meta={NodeMeta.CATEGORY: 'Common', NodeMeta.KEYWORDS: []})
    def clearConsole():
        '''Cross platform clears console.'''
        system = platform.system()
        if system != "":
            system = system.lower()
            if system == "Windows":
                os.system('cls')
            elif system in ("Linux", "Darwin"):
                os.system('clear')

    @staticmethod
    @IMPLEMENT_NODE(returns=('IntPin', 0), meta={NodeMeta.CATEGORY: 'GenericTypes', NodeMeta.KEYWORDS: []})
    def makeInt(i=('IntPin', 0)):
        '''Make integer.'''
        return i

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'GenericTypes', NodeMeta.KEYWORDS: []})
    def makeFloat(f=('FloatPin', 0.0)):
        '''Make floating point number.'''
        return f

    @staticmethod
    @IMPLEMENT_NODE(returns=('StringPin', ''), meta={NodeMeta.CATEGORY: 'GenericTypes', NodeMeta.KEYWORDS: []})
    def makeString(s=('StringPin', '')):
        '''Make string.'''
        return s

    @staticmethod
    @IMPLEMENT_NODE(returns=None, nodeType=NodeTypes.Callable, meta={NodeMeta.CATEGORY: 'Common', NodeMeta.KEYWORDS: []})
    def setGlobalVar(name=('StringPin', 'var1'), value=('AnyPin', None, PIN_ALLOWS_ANYTHING.copy())):
        '''Sets value to globals() dict'''
        globals()[name] = value

    @staticmethod
    @IMPLEMENT_NODE(returns=('AnyPin', None, PIN_ALLOWS_ANYTHING.copy()), meta={NodeMeta.CATEGORY: 'Common', NodeMeta.KEYWORDS: []})
    def getAttribute(obj=('AnyPin', None, PIN_ALLOWS_ANYTHING.copy()), name=('StringPin', 'attrName')):
        '''Returns attribute from object using "getattr(name)"'''
        return getattr(obj, name)

    @staticmethod
    @IMPLEMENT_NODE(returns=('AnyPin', None, PIN_ALLOWS_ANYTHING.copy()), meta={NodeMeta.CATEGORY: 'Common', NodeMeta.KEYWORDS: [],NodeMeta.CACHE_ENABLED: False})
    def getGlobalVar(name=('StringPin', 'var1')):
        '''Retrieves value from globals()'''
        if name in globals():
            return globals()[name]
        else:
            return None

    @staticmethod
    @IMPLEMENT_NODE(returns=('StringPin', ''), meta={NodeMeta.CATEGORY: 'GenericTypes', NodeMeta.KEYWORDS: []})
    def makePath(path=('StringPin', '', {PinSpecifiers.INPUT_WIDGET_VARIANT: "PathWidget"})):
        '''Make path.'''
        return path

    @staticmethod
    @IMPLEMENT_NODE(returns=('BoolPin', False), meta={NodeMeta.CATEGORY: 'GenericTypes', NodeMeta.KEYWORDS: []})
    def makeBool(b=('BoolPin', False)):
        '''Make boolean.'''
        return b

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0, {PinSpecifiers.ENABLED_OPTIONS: PinOptions.AlwaysPushDirty}), meta={NodeMeta.CATEGORY: 'Utils', NodeMeta.KEYWORDS: [], NodeMeta.CACHE_ENABLED: False})
    def clock():
        '''Returns the CPU time or real time since the start of the process or since the first call of process_time().'''
        return currentProcessorTime()

    @staticmethod
    @IMPLEMENT_NODE(returns=('AnyPin', None, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny | PinOptions.DictElementSupported}), meta={NodeMeta.CATEGORY: 'DefaultLib', NodeMeta.KEYWORDS: []})
    def select(A=('AnyPin', None, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny | PinOptions.DictElementSupported}),
               B=('AnyPin', None, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny | PinOptions.DictElementSupported}),
               PickA=('BoolPin', False),
               aPicked=(REF, ("BoolPin", False))):
        '''
        If bPickA is true, A is returned, otherwise B.
        '''
        aPicked(PickA)
        return A if PickA else B

    @staticmethod
    @IMPLEMENT_NODE(returns=('StringPin', ""), meta={NodeMeta.CATEGORY: 'GenericTypes', NodeMeta.KEYWORDS: []})
    def objectType(obj=("AnyPin", None, {PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny | PinOptions.DictElementSupported})):
        '''Returns ``type(obj).__name__``'''
        t = type(obj).__name__
        if t == "DictElement":
            t += ",key:{0},value:{1}".format(type(obj[1]).__name__, type(obj[0]).__name__)
        return t

    @staticmethod
    @IMPLEMENT_NODE(returns=('BoolPin', False), meta={NodeMeta.CATEGORY: 'DefaultLib', NodeMeta.KEYWORDS: ['in']})
    def contains(obj=('AnyPin', None, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny | PinOptions.DictElementSupported}), element=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"})):
        """Python's <b>in</b> keyword. `element in obj` will be executed"""
        try:
            return element in obj
        except:
            return False

    @staticmethod
    @IMPLEMENT_NODE(returns=('IntPin', 0, {PinSpecifiers.DESCRIPTION: "Number of elements of iterable"}), meta={NodeMeta.CATEGORY: 'DefaultLib', NodeMeta.KEYWORDS: ['len']})
    def len(obj=('AnyPin', None, {PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny | PinOptions.DictElementSupported, PinSpecifiers.DESCRIPTION: "Iterable object"})):
        """Python's <b>len</b> function."""
        try:
            return len(obj)
        except:
            return -1

    @staticmethod
    @IMPLEMENT_NODE(returns=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny}),
                    meta={NodeMeta.CATEGORY: 'DefaultLib', NodeMeta.KEYWORDS: ['get']})
    def getItem(obj=('AnyPin', None, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny}),
                element=("AnyPin", None, {PinSpecifiers.SUPPORTED_DATA_TYPES: getHashableDataTypes()}),
                result=(REF, ("BoolPin", False))):
        """Python's <code>[]</code> operator. <code>obj[element]</code> will be executed."""
        try:
            result(True)
            return obj[element]
        except:
            result(False)
            return None

    @staticmethod
    @IMPLEMENT_NODE(returns=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.STRUCT_CONSTRAINT: "1", PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny}), meta={NodeMeta.CATEGORY: 'DefaultLib', NodeMeta.KEYWORDS: ['get']})
    def appendTo(obj=('AnyPin', None, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.STRUCT_CONSTRAINT: "1", PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny}),
                 element=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"}),
                 result=(REF, ('BoolPin', False))):
        """Calls <code>obj.append(element)</code>. And returns object. If failed - object is unchanged"""
        try:
            obj.append(element)
            result(True)
            return obj
        except:
            result(False)
            return obj

    @staticmethod
    @IMPLEMENT_NODE(returns=('BoolPin', False), meta={NodeMeta.CATEGORY: 'DefaultLib', NodeMeta.KEYWORDS: ['get']})
    def addTo(obj=('AnyPin', None, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.STRUCT_CONSTRAINT: "1", PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny}),
              element=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"}),
              result=(REF, ("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.STRUCT_CONSTRAINT: "1", PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny}))):
        """Calls <code>obj.add(element)</code>. And returns object. If failed - object is unchanged"""
        try:
            obj.add(element)
            result(obj)
            return True
        except:
            result(obj)
            return False
