"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core import(
    FunctionLibraryBase,
    IMPLEMENT_NODE
)
from trails.core.common import PinOptions, PinSpecifiers, NodeMeta, REF, clearList


class ArrayLib(FunctionLibraryBase):
    '''doc string for ArrayLib'''
    def __init__(self, packageName):
        super(ArrayLib, self).__init__(packageName)

    @staticmethod
    @IMPLEMENT_NODE(returns=('AnyPin', [], {PinSpecifiers.CONSTRAINT: '1'}), meta={NodeMeta.CATEGORY: 'Array', NodeMeta.KEYWORDS: []})
    def extendArray(lhs=('AnyPin', [], {PinSpecifiers.CONSTRAINT: '1', PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny}),
                    rhs=('AnyPin', [], {PinSpecifiers.CONSTRAINT: '1', PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny})):
        """Extend the list by appending all the items from the iterable."""
        lhs.extend(rhs)
        return lhs

    @staticmethod
    @IMPLEMENT_NODE(returns=('AnyPin', [], {PinSpecifiers.CONSTRAINT: '1', PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny}),
                    meta={NodeMeta.CATEGORY: 'Array', NodeMeta.KEYWORDS: []})
    def insertToArray(ls=('AnyPin', [], {PinSpecifiers.CONSTRAINT: '1', PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny}),
                      elem=('AnyPin', None, {PinSpecifiers.CONSTRAINT: '1'}),
                      index=('IntPin', 0)):
        """Insert an item at a given position. The first argument is the index of the element before which to insert."""
        ls.insert(index, elem)
        return ls

    @staticmethod
    @IMPLEMENT_NODE(returns=("AnyPin", [], {PinSpecifiers.CONSTRAINT: '1', PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny}),
                    meta={NodeMeta.CATEGORY: 'Array', NodeMeta.KEYWORDS: []})
    def removeFromArray(ls=('AnyPin', [], {PinSpecifiers.CONSTRAINT: '1', PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny}),
                        elem=('AnyPin', None, {PinSpecifiers.CONSTRAINT: '1'}),
                        removed=(REF, ('BoolPin', False))):
        """Remove the first item from the list whose value is equal to x."""
        if elem not in ls:
            removed(False)
            return
        ls.remove(elem)
        removed(True)
        return ls

    @staticmethod
    @IMPLEMENT_NODE(returns=("AnyPin", None, {PinSpecifiers.CONSTRAINT: '1'}), meta={NodeMeta.CATEGORY: 'Array', NodeMeta.KEYWORDS: []})
    def popFromArray(ls=('AnyPin', [], {PinSpecifiers.CONSTRAINT: '1', PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny}),
                     index=('IntPin', -1),
                     popped=(REF, ('BoolPin', False)),
                     outLs=(REF, ('AnyPin', [], {PinSpecifiers.CONSTRAINT: '1', PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny}))):
        """Remove the item at the given position in the array, and return it. If no index is specified, ``a.pop()`` removes and returns the last item in the list."""
        poppedElem = None
        try:
            poppedElem = ls.pop(index)
            popped(True)
        except:
            popped(False)
        outLs(ls)

        return poppedElem if poppedElem is not None else 0

    @staticmethod
    @IMPLEMENT_NODE(returns=('AnyPin', [], {PinSpecifiers.CONSTRAINT: '1', PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny}),
                    meta={NodeMeta.CATEGORY: 'Array', NodeMeta.KEYWORDS: []})
    def clearArray(ls=('AnyPin', [], {PinSpecifiers.CONSTRAINT: '1', PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny})):
        """Remove all items from the list."""
        return clearList(ls)

    @staticmethod
    @IMPLEMENT_NODE(returns=('IntPin', 0), meta={NodeMeta.CATEGORY: 'Array', NodeMeta.KEYWORDS: ['in']})
    def arrayElementIndex(ls=('AnyPin', [], {PinSpecifiers.CONSTRAINT: '1', PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny}),
                          element=("AnyPin", None, {PinSpecifiers.CONSTRAINT: '1'}),
                          result=(REF, ("BoolPin", False))):
        """Returns index of array element if it present. If element is not in array -1 will be returned."""
        if element in ls:
            result(True)
            return ls.index(element)
        else:
            result(False)
            return -1

    @staticmethod
    @IMPLEMENT_NODE(returns=('IntPin', 0), meta={NodeMeta.CATEGORY: 'Array', NodeMeta.KEYWORDS: ['in']})
    def arrayElementCount(ls=('AnyPin', [], {PinSpecifiers.CONSTRAINT: '1', PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny}),
                          element=("AnyPin", None, {PinSpecifiers.CONSTRAINT: '1', PinSpecifiers.ENABLED_OPTIONS: PinOptions.AllowAny}),
                          result=(REF, ("BoolPin", False))):
        """Returns len of passed array."""
        if element in ls:
            result(True)
            return ls.count(element)
        else:
            result(False)
            return 0

    @staticmethod
    @IMPLEMENT_NODE(returns=('AnyPin', None, {PinSpecifiers.CONSTRAINT: '1'}), meta={NodeMeta.CATEGORY: 'Array', NodeMeta.KEYWORDS: []})
    def arraySum(Value=('AnyPin', [], {PinSpecifiers.CONSTRAINT: '1', PinSpecifiers.SUPPORTED_DATA_TYPES: ["FloatPin", "IntPin"]})):
        """Python <b>sum()</b> function."""
        return sum(Value)

    @staticmethod
    @IMPLEMENT_NODE(returns=('AnyPin', [], {PinSpecifiers.CONSTRAINT: '1', PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny}),
                    meta={NodeMeta.CATEGORY: 'Array', NodeMeta.KEYWORDS: ['in']})
    def arraySlice(ls=('AnyPin', [], {PinSpecifiers.CONSTRAINT: '1', PinSpecifiers.ENABLED_OPTIONS: PinOptions.ArraySupported | PinOptions.AllowAny}),
                   start=("IntPin", 0),
                   end=("IntPin", 1),
                   result=(REF, ("BoolPin", False))):
        """Array slice."""
        try:
            result(True)
            return ls[start:end]
        except:
            result(False)
            return ls
