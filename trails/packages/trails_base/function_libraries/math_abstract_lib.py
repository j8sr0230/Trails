"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core import(
    FunctionLibraryBase,
    IMPLEMENT_NODE
)
from trails.core.common import PinSpecifiers, NodeMeta, REF, mapRangeClamped, mapRangeUnclamped, clamp


class MathAbstractLib(FunctionLibraryBase):
    """doc string for MathAbstractLib"""
    def __init__(self, packageName):
        super(MathAbstractLib, self).__init__(packageName)

    @staticmethod
    @IMPLEMENT_NODE(returns=("BoolPin", False), meta={NodeMeta.CATEGORY: 'Math|Basic', NodeMeta.KEYWORDS: ["=", "operator"]})
    ## Is a equal b
    def isEqual(a=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"}),
                b=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"})):
        """Is a equal b."""
        return a == b

    @staticmethod
    @IMPLEMENT_NODE(returns=("BoolPin", False), meta={NodeMeta.CATEGORY: 'Math|Basic', NodeMeta.KEYWORDS: [">", "operator"]})
    def isGreater(a=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"}),
                  b=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"}),
                  result=(REF, ("BoolPin", False))):
        """Operator <b>&gt;</b>."""
        return a > b

    @staticmethod
    @IMPLEMENT_NODE(returns=("BoolPin", False), meta={NodeMeta.CATEGORY: 'Math|Basic', NodeMeta.KEYWORDS: [">", "operator"]})
    def isGreaterOrEqual(a=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"}),
                         b=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"}),
                         result=(REF, ("BoolPin", False))):
        """Operator <b>&gt;=</b>."""
        return a >= b

    @staticmethod
    @IMPLEMENT_NODE(returns=("BoolPin", False), meta={NodeMeta.CATEGORY: 'Math|Basic', NodeMeta.KEYWORDS: ["<", "operator"]})
    def isLess(a=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"}), b=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"}),
               result=(REF, ("BoolPin", False))):
        """Operator <b>&lt;</b>."""
        return a < b

    @staticmethod
    @IMPLEMENT_NODE(returns=("BoolPin", False), meta={NodeMeta.CATEGORY: 'Math|Basic', NodeMeta.KEYWORDS: ["<", "operator"]})
    def isLessOrEqual(a=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"}), b=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"})):
        """Operator <b>&lt;=</b>."""
        return a <= b

    @staticmethod
    @IMPLEMENT_NODE(returns=(("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"})), meta={NodeMeta.CATEGORY: 'Math|Basic', NodeMeta.KEYWORDS: ['+', 'append', "sum", "operator"]})
    def add(a=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"}), b=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"})):
        """Operator <b>+</b>."""
        return a + b

    @staticmethod
    @IMPLEMENT_NODE(returns=(("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"})), meta={NodeMeta.CATEGORY: 'Math|Basic', NodeMeta.KEYWORDS: ['-', "operator", "minus"]})
    def subtract(a=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"}), b=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"})):
        """Operator <b>-</b>."""
        return a - b

    @staticmethod
    @IMPLEMENT_NODE(returns=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"}), meta={NodeMeta.CATEGORY: 'Math|Basic', NodeMeta.KEYWORDS: ['/', "divide", "operator"]})
    def divide(a=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"}), b=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"})):
        """Operator <b>/</b>."""
        return a / b

    @staticmethod
    @IMPLEMENT_NODE(returns=(("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"})), meta={NodeMeta.CATEGORY: 'Math|Basic', NodeMeta.KEYWORDS: ['*', "multiply", "operator"]})
    def multiply(a=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"}), b=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1"})):
        """Operator <b>*</b>."""
        return a * b

    @staticmethod
    @IMPLEMENT_NODE(returns=("BoolPin", False), meta={NodeMeta.CATEGORY: 'Math|Basic', NodeMeta.KEYWORDS: ["in", "range"]})
    def inRange(Value=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.SUPPORTED_DATA_TYPES: ["FloatPin", "IntPin"]}),
                RangeMin=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.SUPPORTED_DATA_TYPES: ["FloatPin", "IntPin"]}),
                RangeMax=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.SUPPORTED_DATA_TYPES: ["FloatPin", "IntPin"]}),
                InclusiveMin=("BoolPin", False),
                InclusiveMax=("BoolPin", False)):
        """Returns true if value is between Min and Max (V >= Min && V <= Max) If InclusiveMin is true, value needs to be equal or larger than Min,<br/>
             else it needs to be larger If InclusiveMax is true, value needs to be smaller or equal than Max, else it needs to be smaller
        """
        return ((Value >= RangeMin) if InclusiveMin else (Value > RangeMin)) and ((Value <= RangeMax) if InclusiveMax else (Value < RangeMax))

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Math|Basic', NodeMeta.KEYWORDS: []})
    def mapRangeClamped(Value=("FloatPin", 0.0),
                        InRangeA=("FloatPin", 0.0),
                        InRangeB=("FloatPin", 0.0),
                        OutRangeA=("FloatPin", 0.0),
                        OutRangeB=("FloatPin", 0.0)):
        """Returns Value mapped from one range into another where the Value is clamped to the Input Range.<br/>
             (e.g. 0.5 normalized from the range 0->1 to 0->50 would result in 25)"""
        return mapRangeClamped(Value, InRangeA, InRangeB, OutRangeA, OutRangeB)

    @staticmethod
    @IMPLEMENT_NODE(returns=('FloatPin', 0.0), meta={NodeMeta.CATEGORY: 'Math|Basic', NodeMeta.KEYWORDS: []})
    def mapRangeUnclamped(Value=("FloatPin", 0.0),
                          InRangeA=("FloatPin", 0.0),
                          InRangeB=("FloatPin", 0.0),
                          OutRangeA=("FloatPin", 0.0),
                          OutRangeB=("FloatPin", 0.0)):
        """Returns Value mapped from one range into another where the Value is clamped to the Input Range.<br/>
             (e.g. 0.5 normalized from the range 0->1 to 0->50 would result in 25)"""
        return mapRangeUnclamped(Value, InRangeA, InRangeB, OutRangeA, OutRangeB)

    @staticmethod
    @IMPLEMENT_NODE(returns=("FloatPin", 0.0), meta={NodeMeta.CATEGORY: 'Math|Basic', NodeMeta.KEYWORDS: ['clamp']})
    def clamp(i=("FloatPin", 0.0),
              imin=("FloatPin", 0.0),
              imax=("FloatPin", 0.0)):
        """Clamp."""
        return clamp(i, imin, imax)

    @staticmethod
    @IMPLEMENT_NODE(returns=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.SUPPORTED_DATA_TYPES: ["FloatPin", "IntPin"]}), meta={NodeMeta.CATEGORY: 'Math|Basic', NodeMeta.KEYWORDS: ["operator"]})
    def modulo(a=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.SUPPORTED_DATA_TYPES: ["FloatPin", "IntPin"]}),
               b=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.SUPPORTED_DATA_TYPES: ["FloatPin", "IntPin"]})):
        """Modulo (A % B)."""
        return a % b

    @staticmethod
    @IMPLEMENT_NODE(returns=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.SUPPORTED_DATA_TYPES: ["FloatPin", "IntPin"]}), meta={NodeMeta.CATEGORY: 'Math|Basic', NodeMeta.KEYWORDS: []})
    def abs(inp=("AnyPin", None, {PinSpecifiers.CONSTRAINT: "1", PinSpecifiers.SUPPORTED_DATA_TYPES: ["FloatPin", "IntPin"]})):
        """Return the absolute value of a number."""
        return abs(inp)
