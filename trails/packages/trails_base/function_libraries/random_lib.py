"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import random

from trails.core import(
    FunctionLibraryBase,
    IMPLEMENT_NODE
)
from trails.core.common import PinSpecifiers, NodeMeta, REF


class RandomLib(FunctionLibraryBase):
    """doc string for RandomLib"""

    def __init__(self, packageName):
        super(RandomLib, self).__init__(packageName)

    @staticmethod
    @IMPLEMENT_NODE(returns=None, meta={NodeMeta.CATEGORY: 'Math|random', NodeMeta.KEYWORDS: [], NodeMeta.CACHE_ENABLED: False})
    # Return a random integer N such that a <= N <= b
    def randint(start=('IntPin', 0), end=('IntPin', 10), Result=(REF, ('IntPin', 0))):
        """Return a random integer N such that a <= N <= b."""
        Result(random.randint(start, end))

    @staticmethod
    @IMPLEMENT_NODE(returns=None, meta={NodeMeta.CATEGORY: 'Math|random', NodeMeta.KEYWORDS: []})
    # Shuffle the sequence x in place
    def shuffle(seq=('AnyPin', [], {PinSpecifiers.CONSTRAINT: "1"}), Result=(REF, ('AnyPin', [], {PinSpecifiers.CONSTRAINT: "1"}))):
        """Shuffle the sequence x in place."""
        random.shuffle(seq)
        Result(seq)
