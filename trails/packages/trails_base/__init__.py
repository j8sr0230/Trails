"""Base package.

Copyright (C) 2015-2019  ?? Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

PACKAGE_NAME = 'TrailsBase'
from collections import OrderedDict

from trails.ui.ui_interfaces import IPackage

# Pins
from trails.packages.trails_base.pins.any_pin import AnyPin
from trails.packages.trails_base.pins.bool_pin import BoolPin
from trails.packages.trails_base.pins.exec_pin import ExecPin
from trails.packages.trails_base.pins.float_pin import FloatPin
from trails.packages.trails_base.pins.int_pin import IntPin
from trails.packages.trails_base.pins.string_pin import StringPin

# Function based nodes
from trails.packages.trails_base.function_libraries.array_lib import ArrayLib
from trails.packages.trails_base.function_libraries.bool_lib import BoolLib
from trails.packages.trails_base.function_libraries.default_lib import DefaultLib
from trails.packages.trails_base.function_libraries.float_lib import FloatLib
from trails.packages.trails_base.function_libraries.int_lib import IntLib
from trails.packages.trails_base.function_libraries.math_lib import MathLib
from trails.packages.trails_base.function_libraries.math_abstract_lib import MathAbstractLib
from trails.packages.trails_base.function_libraries.random_lib import RandomLib
from trails.packages.trails_base.function_libraries.path_lib import PathLib

# Class based nodes
from trails.packages.trails_base.nodes.branch import branch
from trails.packages.trails_base.nodes.tick import tick
from trails.packages.trails_base.nodes.charge import charge
from trails.packages.trails_base.nodes.delay import delay
from trails.packages.trails_base.nodes.delta_time import deltaTime
from trails.packages.trails_base.nodes.do_n import doN
from trails.packages.trails_base.nodes.do_once import doOnce
from trails.packages.trails_base.nodes.flip_flop import flipFlop
from trails.packages.trails_base.nodes.for_loop import forLoop
from trails.packages.trails_base.nodes.for_loop_begin import forLoopBegin
from trails.packages.trails_base.nodes.loop_end import loopEnd
from trails.packages.trails_base.nodes.while_loop_begin import whileLoopBegin
from trails.packages.trails_base.nodes.for_each_loop import forEachLoop
from trails.packages.trails_base.nodes.for_loop_with_break import forLoopWithBreak
from trails.packages.trails_base.nodes.retriggerable_delay import retriggerableDelay
from trails.packages.trails_base.nodes.sequence import sequence
from trails.packages.trails_base.nodes.switch_on_string import switchOnString
from trails.packages.trails_base.nodes.timer import timer
from trails.packages.trails_base.nodes.while_loop import whileLoop
from trails.packages.trails_base.nodes.get_var import getVar
from trails.packages.trails_base.nodes.set_var import setVar
from trails.packages.trails_base.nodes.reroute import reroute
from trails.packages.trails_base.nodes.reroute_execs import rerouteExecs
from trails.packages.trails_base.nodes.make_array import makeArray
from trails.packages.trails_base.nodes.make_list import makeList
from trails.packages.trails_base.nodes.make_dict import makeDict
from trails.packages.trails_base.nodes.make_any_dict import makeAnyDict
from trails.packages.trails_base.nodes.make_dict_element import makeDictElement
from trails.packages.trails_base.nodes.dict_keys import dictKeys
from trails.packages.trails_base.nodes.float_ramp import floatRamp
from trails.packages.trails_base.nodes.color_ramp import colorRamp
from trails.packages.trails_base.nodes.string_to_array import stringToArray
from trails.packages.trails_base.nodes.cliexit import cliexit


from trails.packages.trails_base.nodes.console_output import consoleOutput
from trails.packages.trails_base.nodes.address import address
from trails.packages.trails_base.nodes.graph_nodes import graphInputs, graphOutputs
from trails.packages.trails_base.nodes.python_node import pythonNode
from trails.packages.trails_base.nodes.compound import compound
from trails.packages.trails_base.nodes.constant import constant
from trails.packages.trails_base.nodes.convert_to import convertTo
from trails.packages.trails_base.nodes.image_display import imageDisplay


from trails.packages.trails_base.nodes.comment_node import commentNode
from trails.packages.trails_base.nodes.sticky_note import stickyNote

from trails.packages.trails_base.tools.screenshot_tool import ScreenshotTool
from trails.packages.trails_base.tools.node_box_tool import NodeBoxTool
from trails.packages.trails_base.tools.search_results_tool import SearchResultsTool
from trails.packages.trails_base.tools.align_left_tool import AlignLeftTool
from trails.packages.trails_base.tools.align_right_tool import AlignRightTool
from trails.packages.trails_base.tools.align_top_tool import AlignTopTool
from trails.packages.trails_base.tools.align_bottom_tool import AlignBottomTool
from trails.packages.trails_base.tools.history_tool import HistoryTool
from trails.packages.trails_base.tools.properties_tool import PropertiesTool
from trails.packages.trails_base.tools.variables_tool import VariablesTool
from trails.packages.trails_base.tools.compile_tool import CompileTool
from trails.packages.trails_base.tools.logger_tool import LoggerTool

from trails.packages.trails_base.exporters.python_script_exporter import PythonScriptExporter

# Factories
from trails.packages.trails_base.factories.ui_pin_factory import createUIPin
from trails.packages.trails_base.factories import pin_input_widget_factory
from trails.packages.trails_base.factories.ui_node_factory import createUINode

# Prefs widgets
from trails.packages.trails_base.prefs_widgets.general_prefs import GeneralPreferences
from trails.packages.trails_base.prefs_widgets.shortcuts_prefs import ShortcutsPreferences
from trails.packages.trails_base.prefs_widgets.theme_prefs import UIThemePreferences
from trails.packages.trails_base.prefs_widgets.theme_prefs import GraphThemePreferences


_FOO_LIBS = {
    ArrayLib.__name__: ArrayLib(PACKAGE_NAME),
    BoolLib.__name__: BoolLib(PACKAGE_NAME),
    DefaultLib.__name__: DefaultLib(PACKAGE_NAME),
    FloatLib.__name__: FloatLib(PACKAGE_NAME),
    IntLib.__name__: IntLib(PACKAGE_NAME),
    MathLib.__name__: MathLib(PACKAGE_NAME),
    MathAbstractLib.__name__: MathAbstractLib(PACKAGE_NAME),
    RandomLib.__name__: RandomLib(PACKAGE_NAME),
    PathLib.__name__: PathLib(PACKAGE_NAME),
}


_NODES = {
    branch.__name__: branch,
    charge.__name__: charge,
    delay.__name__: delay,
    deltaTime.__name__: deltaTime,
    doN.__name__: doN,
    doOnce.__name__: doOnce,
    flipFlop.__name__: flipFlop,
    forLoop.__name__: forLoop,
    forLoopBegin.__name__: forLoopBegin,
    loopEnd.__name__: loopEnd,
    forLoopWithBreak.__name__: forLoopWithBreak,
    retriggerableDelay.__name__: retriggerableDelay,
    sequence.__name__: sequence,
    switchOnString.__name__: switchOnString,
    timer.__name__: timer,
    whileLoop.__name__: whileLoop,
    whileLoopBegin.__name__: whileLoopBegin,
    commentNode.__name__: commentNode,
    stickyNote.__name__: stickyNote,
    getVar.__name__: getVar,
    setVar.__name__: setVar,
    reroute.__name__: reroute,
    rerouteExecs.__name__: rerouteExecs,
    graphInputs.__name__: graphInputs,
    graphOutputs.__name__: graphOutputs,
    compound.__name__: compound,
    pythonNode.__name__: pythonNode,
    makeArray.__name__: makeArray,
    makeList.__name__: makeList,
    makeDict.__name__: makeDict,
    makeAnyDict.__name__: makeAnyDict,
    makeDictElement.__name__: makeDictElement,
    consoleOutput.__name__: consoleOutput,
    forEachLoop.__name__: forEachLoop,
    address.__name__: address,
    constant.__name__: constant,
    tick.__name__: tick,
    convertTo.__name__: convertTo,
    dictKeys.__name__: dictKeys,
    floatRamp.__name__: floatRamp,
    colorRamp.__name__: colorRamp,
    stringToArray.__name__: stringToArray,
    imageDisplay.__name__: imageDisplay,
    cliexit.__name__: cliexit
}

_PINS = {
    AnyPin.__name__: AnyPin,
    BoolPin.__name__: BoolPin,
    ExecPin.__name__: ExecPin,
    FloatPin.__name__: FloatPin,
    IntPin.__name__: IntPin,
    StringPin.__name__: StringPin,
}

# Toolbar will be created in following order
_TOOLS = OrderedDict()
_TOOLS[CompileTool.__name__] = CompileTool
_TOOLS[ScreenshotTool.__name__] = ScreenshotTool
_TOOLS[AlignLeftTool.__name__] = AlignLeftTool
_TOOLS[AlignRightTool.__name__] = AlignRightTool
_TOOLS[AlignTopTool.__name__] = AlignTopTool
_TOOLS[AlignBottomTool.__name__] = AlignBottomTool
_TOOLS[HistoryTool.__name__] = HistoryTool
_TOOLS[PropertiesTool.__name__] = PropertiesTool
_TOOLS[VariablesTool.__name__] = VariablesTool
_TOOLS[NodeBoxTool.__name__] = NodeBoxTool
_TOOLS[SearchResultsTool.__name__] = SearchResultsTool
_TOOLS[LoggerTool.__name__] = LoggerTool

_EXPORTERS = OrderedDict()
_EXPORTERS[PythonScriptExporter.__name__] = PythonScriptExporter


_PREFS_WIDGETS = OrderedDict()
_PREFS_WIDGETS["General"] = GeneralPreferences
_PREFS_WIDGETS["Shortcuts"] = ShortcutsPreferences
_PREFS_WIDGETS["UI"] = UIThemePreferences
_PREFS_WIDGETS["Graph"] = GraphThemePreferences


class TrailsBase(IPackage):
    """Base Trails package
    """
    def __init__(self):
        super(TrailsBase, self).__init__()

    @staticmethod
    def GetExporters():
        return _EXPORTERS

    @staticmethod
    def GetFunctionLibraries():
        return _FOO_LIBS

    @staticmethod
    def GetNodeClasses():
        return _NODES

    @staticmethod
    def GetPinClasses():
        return _PINS

    @staticmethod
    def GetToolClasses():
        return _TOOLS

    @staticmethod
    def UIPinsFactory():
        return createUIPin

    @staticmethod
    def UINodesFactory():
        return createUINode

    @staticmethod
    def PinsInputWidgetFactory():
        return pin_input_widget_factory.getInputWidget

    @staticmethod
    def PrefsWidgets():
        return _PREFS_WIDGETS
