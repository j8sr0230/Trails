"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt import QtCore
from Qt import QtGui
from Qt.QtWidgets import QComboBox, QCheckBox

from trails.ui.canvas.ui_node_base import UINodeBase
from trails import findPinClassByType
from trails.config_manager import GraphThemeManager


class UIConvertToNode(UINodeBase):
    def __init__(self, raw_node):
        super(UIConvertToNode, self).__init__(raw_node)
        self.headColorOverride = GraphThemeManager().NodeHeadAlt
        self.color = GraphThemeManager().NodeAlt
        self.headColor = self.headColorOverride = QtGui.QColor(
            *findPinClassByType("AnyPin").color())
        if self.headColor.lightnessF() > 0.75:
            self.labelTextColor = QtCore.Qt.black
        else:
            self.labelTextColor = QtCore.Qt.white
        self.prevDataType = "AnyPin"

    def postCreate(self, jsonTemplate=None):
        super(UIConvertToNode, self).postCreate(jsonTemplate)
        self.output = self.getPinSG("result")
        self.output.OnPinChanged.connect(self.changeOnConection)
        self.changeType(self.output.dataType)
        self.updateNodeShape()

    def changeOnConection(self, other):
        if other.dataType != self.prevDataType:
            self.prevDataType = other.dataType
            self.changeType(other.dataType)

    def changeType(self, dataType):
        self.headColor = self.headColorOverride = QtGui.QColor(
            *findPinClassByType(dataType).color())
        if self.headColor.lightnessF() > 0.75:
            self.labelTextColor = QtCore.Qt.black
        else:
            self.labelTextColor = QtCore.Qt.white
        self.update()
        self.canvasRef().tryFillPropertiesView(self)

    def createInputWidgets(self, inputsCategory, group=None, pins=True):
        preIndex = inputsCategory.Layout.count()
        if pins:
            super(UIConvertToNode, self).createInputWidgets(inputsCategory, group)
        selector = QComboBox()
        for i in self._rawNode.pinTypes:
            selector.addItem(i)
        if self.output.dataType in self._rawNode.pinTypes:
            selector.setCurrentIndex(
                self._rawNode.pinTypes.index(self.output.dataType))

        selector.activated.connect(self._rawNode.updateType)
        inputsCategory.insertWidget(preIndex, "DataType", selector, group=group)
