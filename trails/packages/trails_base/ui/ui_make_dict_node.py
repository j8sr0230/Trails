"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt.QtWidgets import QComboBox
from trails.ui.canvas.ui_node_base import UINodeBase


class UIMakeDictNode(UINodeBase):
    def __init__(self, raw_node):
        super(UIMakeDictNode, self).__init__(raw_node)
        self.prevDataType = "AnyPin"

    def postCreate(self, jsonTemplate=None):
        super(UIMakeDictNode, self).postCreate(jsonTemplate)
        self.input = self.getPinSG("KeyType")

    def changeType(self, dataType):
        self.input._rawPin.initType(
            self.input._rawPin._defaultSupportedDataTypes[dataType], True)

    def selectStructure(self, name):
        self.canvasRef().tryFillPropertiesView(self)

    def createInputWidgets(self, inputsCategory, inGroup=None, pins=True):
        if pins:
            super(UIMakeDictNode, self).createInputWidgets(inputsCategory, inGroup)
        selector = QComboBox()
        for i in self.input._rawPin._defaultSupportedDataTypes:
            selector.addItem(i)

        selector.setCurrentIndex(self.input._rawPin._defaultSupportedDataTypes.index(
            self.input._rawPin.dataType))

        selector.activated.connect(self.changeType)
        inputsCategory.insertWidget(0, "DataType", selector, group=inGroup)
