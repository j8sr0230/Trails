"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt import (
    QtGui,
    QtCore
)

from trails.core import PinBase
from trails.ui.canvas.ui_pin_base import UIPinBase
from trails.ui.canvas.painters import PinPainter


class UIExecPin(UIPinBase):
    def __init__(self, owningNode, raw_pin):
        super(UIExecPin, self).__init__(owningNode, raw_pin)

    def paint(self, painter, option, widget):
        # PinPainter.asValuePin(self, painter, option, widget)
        PinPainter.asExecPin(self, painter, option, widget)

    def hoverEnterEvent(self, event):
        super(UIPinBase, self).hoverEnterEvent(event)
        self.update()
        self.hovered = True
        event.accept()
