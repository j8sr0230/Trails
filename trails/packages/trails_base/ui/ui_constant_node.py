"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt import QtCore
from Qt import QtGui
from Qt.QtWidgets import QComboBox, QCheckBox

from trails.core.common import StructureType
from trails.ui.canvas.ui_node_base import UINodeBase
from trails import findPinClassByType
from trails.config_manager import GraphThemeManager


class UIConstantNode(UINodeBase):
    def __init__(self, raw_node):
        super(UIConstantNode, self).__init__(raw_node)
        self.hover = False
        self.headColorOverride = GraphThemeManager().NodeHeadAlt
        self.color = GraphThemeManager().NodeAlt
        self.headColor = self.headColorOverride = QtGui.QColor(
            *findPinClassByType("AnyPin").color())
        if self.headColor.lightnessF() > 0.75:
            self.labelTextColor = QtCore.Qt.black
        else:
            self.labelTextColor = QtCore.Qt.white
        self.prevDataType = "AnyPin"

    def kill(self, *args, **kwargs):
        inp = list(self.UIinputs.values())[0]
        out = list(self.UIoutputs.values())[0]
        newOuts = []
        for i in self.UIoutputs.values():
            for connection in i.connections:
                newOuts.append([connection.destination(),
                                connection.drawDestination])
        if inp.connections:
            source = inp.connections[0].source()
            for out in newOuts:
                drawSource = inp.connections[0].drawSource
                self.canvasRef().connectPins(source, out[0])
        super(UIConstantNode, self).kill()

    def postCreate(self, jsonTemplate=None):
        super(UIConstantNode, self).postCreate(jsonTemplate)
        self.input = self.getPinSG("in")
        self.output = self.getPinSG("out")
        self.input.OnPinChanged.connect(self.changeOnConection)
        self.output.OnPinChanged.connect(self.changeOnConection)
        self.changeType(self.input.dataType)
        self.updateNodeShape()

    def changeOnConection(self, other):
        if other.dataType != self.prevDataType:
            self.prevDataType = other.dataType
            self.changeType(other.dataType)

    def changeType(self, dataType):
        self.headColor = self.headColorOverride = QtGui.QColor(
            *findPinClassByType(dataType).color())
        if self.headColor.lightnessF() > 0.75:
            self.labelTextColor = QtCore.Qt.black
        else:
            self.labelTextColor = QtCore.Qt.white
        self.update()

    def updateType(self, valToUpdate, inputsCategory, group):
        if valToUpdate is not None:
            par = valToUpdate.parent().parent()
            del par
            super(UIConstantNode, self).createInputWidgets(inputsCategory, group)

    def selectStructure(self, valToUpdate, inputsCategory, group):
        if valToUpdate is not None:
            del valToUpdate
            super(UIConstantNode, self).createInputWidgets(
                inputsCategory, group)

    def createInputWidgets(self, inputsCategory, inGroup=None, pins=True):
        inputVal = None
        preIndex = inputsCategory.Layout.count()
        if pins:
            super(UIConstantNode, self).createInputWidgets(inputsCategory, inGroup)
            inputVal = inputsCategory.getWidgetByName("in")

        selector = QComboBox()

        for i in self._rawNode.pinTypes:
            selector.addItem(i)
        if self.input.dataType in self._rawNode.pinTypes:
            selector.setCurrentIndex(
                self._rawNode.pinTypes.index(self.input.dataType))

        structSelector = QComboBox()
        for i in [i.name for i in list(StructureType)]:
            structSelector.addItem(i)
        structSelector.inputsCategory = inputsCategory

        structSelector.setCurrentIndex(self.input._rawPin._currStructure)
        selector.activated.connect(self._rawNode.updateType)
        selector.activated.connect(
            lambda: self.updateType(inputVal, inputsCategory, inGroup))
        structSelector.activated.connect(self._rawNode.selectStructure)
        structSelector.activated.connect(
            lambda: self.selectStructure(inputVal, inputsCategory, inGroup))

        inputsCategory.insertWidget(
            preIndex, "DataType", selector, group=inGroup)
        inputsCategory.insertWidget(preIndex + 1, "Structure", structSelector, group=inGroup)
