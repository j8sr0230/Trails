"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import json
import logging
import os

from Qt.QtWidgets import QFileDialog
from Qt.QtWidgets import QInputDialog
from Qt.QtWidgets import QMessageBox

from trails import GET_PACKAGE_PATH, GET_PACKAGES
from trails.ui.canvas.ui_common import validateGraphDataPackages
from trails.ui.canvas.ui_node_base import UINodeBase, getUINodeInstance
from trails.ui import RESOURCES_DIR
from trails.ui.widgets.properties_framework import CollapsibleFormWidget
from trails.ui.editor_history import EditorHistory
from trails.config_manager import GraphThemeManager


logger = logging.getLogger(None)


class UICompoundNode(UINodeBase):
    def __init__(self, raw_node):
        super(UICompoundNode, self).__init__(raw_node)
        self._rawNode.pinExposed.connect(self._createUIPinWrapper)
        self.headColorOverride = GraphThemeManager().NodeHeadAlt
        self.color = GraphThemeManager().NodeAlt
        self.image = RESOURCES_DIR + "/gear.svg"
        self.heartBeatDelay = 1.0

        self.actionStepIn = self._menu.addAction("Step in")
        self.actionStepIn.triggered.connect(self.stepIn)
        self.actionExport = self._menu.addAction("Export")
        self.actionExport.triggered.connect(self.onExport)
        self.actionExport = self._menu.addAction("Export to package")
        self.actionExport.triggered.connect(self.onExportToPackage)
        self.actionImport = self._menu.addAction("Import")
        self.actionImport.triggered.connect(self.onImport)

    def rebuild(self):
        if self._rawNode._rawGraphJson is not None:
            self.assignData(self._rawNode._rawGraphJson)

    def onExport(self, root=None):
        try:
            savePath, selectedFilter = QFileDialog.getSaveFileName(filter="Subgraph data (*.compound)", dir=root)
        except:
            savePath, selectedFilter = QFileDialog.getSaveFileName(filter="Subgraph data (*.compound)")
        if savePath != "":
            with open(savePath, 'w') as f:
                json.dump(self._rawNode.rawGraph.serialize(), f, indent=4)
            logger.info("{0} data successfully exported!".format(self.getName()))

    def onExportToPackage(self):
        # check if category is not empty
        if self._rawNode._rawGraph.category == '':
            QMessageBox.information(None, "Warning", "Category is not set! Please step into compound and type category name.")
            return

        packageNames = list(GET_PACKAGES().keys())
        selectedPackageName, accepted = QInputDialog.getItem(None, "Select", "Select package", packageNames, editable=False)
        if accepted:
            packagePath = GET_PACKAGE_PATH(selectedPackageName)
            compoundsDir = os.path.join(packagePath, "Compounds")
            if not os.path.isdir(compoundsDir):
                os.mkdir(compoundsDir)
            self.onExport(root=compoundsDir)
            # refresh node box
            app = self.canvasRef().getApp()
            nodeBoxes = app.getRegisteredTools(classNameFilters=["NodeBoxTool"])
            for nodeBox in nodeBoxes:
                nodeBox.refresh()

    def onImport(self):
        openPath, selectedFilter = QFileDialog.getOpenFileName(filter="Subgraph data (*.compound)")
        if openPath != "":
            with open(openPath, 'r') as f:
                data = json.load(f)
                self.assignData(data)

    def assignData(self, data):
        data["isRoot"] = False
        data["parentGraphName"] = self._rawNode.rawGraph.parentGraph.name
        missedPackages = set()
        if validateGraphDataPackages(data, missedPackages):
            data["nodes"] = self.canvasRef().makeSerializedNodesUnique(data["nodes"])
            self._rawNode.rawGraph.populateFromJson(data)
            self.canvasRef().createWrappersForGraph(self._rawNode.rawGraph)
            EditorHistory().saveState("Import compound", modify=True)
        else:
            logger.error("Missing dependencies! {0}".format(",".join(missedPackages)))

    def getGraph(self):
        return self._rawNode.rawGraph

    def stepIn(self):
        self._rawNode.graph().graphManager.selectGraph(self._rawNode.rawGraph)

    def mouseDoubleClickEvent(self, event):
        self.stepIn()
        event.accept()

    def kill(self, *args, **kwargs):
        super(UICompoundNode, self).kill()

    def onGraphNameChanged(self, newName):
        self.name = newName
        self.setHeaderHtml(self.name)

    def postCreate(self, jsonTemplate=None):
        super(UICompoundNode, self).postCreate(jsonTemplate)
        self.canvasRef().createWrappersForGraph(self._rawNode.rawGraph)
        self._rawNode.rawGraph.nameChanged.connect(self.onGraphNameChanged)

    def createInputWidgets(self, inputsCategory, inGroup=None, pins=True):
        if pins:
            super(UICompoundNode, self).createInputWidgets(inputsCategory, inGroup)
        nodes = self._rawNode.rawGraph.getNodesList()
        if len(nodes) > 0:
            for node in nodes:
                wrapper = node.getWrapper()
                if wrapper is not None:
                    if wrapper.bExposeInputsToCompound:
                        wrapper.createInputWidgets(inputsCategory, inGroup="{} inputs".format(node.name), pins=False)
