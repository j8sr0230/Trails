"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core.common import getConnectedPins
from trails.ui import RESOURCES_DIR
from trails.ui.canvas.ui_node_base import UINodeBase
from trails.ui.widgets.enum_combo_box import EnumComboBox
from trails.ui.widgets.properties_framework import CollapsibleFormWidget
from trails.config_manager import GraphThemeManager

# Variable setter node


class UISetVarNode(UINodeBase):
    """docstring for UISetVarNode"""

    def __init__(self, raw_node):
        super(UISetVarNode, self).__init__(raw_node)
        self.image = RESOURCES_DIR + "/gear.svg"
        self.headColorOverride = GraphThemeManager().NodeHeadAlt
        self.color = GraphThemeManager().NodeAlt

    @property
    def var(self):
        return self._rawNode.var

    @var.setter
    def var(self, newVar):
        if self.var is not None:
            self.var.nameChanged.disconnect(self.updateHeaderText)
        self._rawNode.var = newVar
        if self.var is not None:
            self.var.nameChanged.connect(self.updateHeaderText)

    def onVariableWasChanged(self):
        self._createUIPinWrapper(self._rawNode.inp)
        self._createUIPinWrapper(self._rawNode.out)

    def serialize(self):
        template = UINodeBase.serialize(self)
        template['meta']['var'] = self.var.serialize()
        return template

    def onVarSelected(self, varName):
        if self.var is not None:
            if self.var.name == varName:
                return
        else:
            self._rawNode.out.disconnectAll()

        var = self.canvasRef().graphManager.findVariableByName(varName)

        if var:
            inLinkedTo = getConnectedPins(self._rawNode.inp)
            outLinkedTo = getConnectedPins(self._rawNode.out)
            self.var = var
            self._rawNode.updateStructure()
            for i in outLinkedTo:
                self.canvasRef().connectPinsInternal(
                    self._rawNode.out.getWrapper()(), i.getWrapper()())

            for o in inLinkedTo:
                self.canvasRef().connectPinsInternal(
                    o.getWrapper()(), self._rawNode.inp.getWrapper()())

            self.updateHeaderText()

    def createInputWidgets(self, inputsCategory, inGroup=None, pins=True):
        inputsCategory.setButtonName("Variable")
        validVars = self.graph().getVarList()
        cbVars = EnumComboBox([v.name for v in validVars])
        if self.var is not None:
            cbVars.setCurrentText(self.var.name)
        else:
            cbVars.setCurrentText("")
        cbVars.changeCallback.connect(self.onVarSelected)
        inputsCategory.addWidget("var", cbVars, group=inGroup)
        if pins:
            super(UISetVarNode, self).createInputWidgets(inputsCategory, inGroup)

    def postCreate(self, template):
        super(UISetVarNode, self).postCreate(template)
        self.var.nameChanged.connect(self.updateHeaderText)
        self.updateHeaderText()

        for pin in self.UIPins.values():
            pin.setMenuItemEnabled("InitAs", False)

    def updateHeaderText(self, name=None):
        self.setHeaderHtml("Set {0}".format(self.var.name))
        self.updateNodeShape()

    @staticmethod
    def category():
        return 'Variables'
