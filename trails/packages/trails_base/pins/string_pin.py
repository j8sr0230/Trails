"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core import PinBase
from trails.core.common import PinSpecifiers


class StringPin(PinBase):
    """doc string for StringPin"""
    def __init__(self, name, parent, direction, **kwargs):
        super(StringPin, self).__init__(name, parent, direction, **kwargs)
        self.setDefaultValue("")

    @staticmethod
    def IsValuePin():
        return True

    def getInputWidgetVariant(self):
        if self.annotationDescriptionDict is not None:
            if PinSpecifiers.VALUE_LIST in self.annotationDescriptionDict:
                return "EnumWidget"
            if PinSpecifiers.INPUT_WIDGET_VARIANT in self.annotationDescriptionDict:
                return self.annotationDescriptionDict[PinSpecifiers.INPUT_WIDGET_VARIANT]
        return self._inputWidgetVariant

    @staticmethod
    def supportedDataTypes():
        return ('StringPin',)

    @staticmethod
    def color():
        return (255, 8, 127, 255)

    @staticmethod
    def pinDataTypeHint():
        return 'StringPin', ''

    @staticmethod
    def internalDataStructure():
        return str

    @staticmethod
    def processData(data):
        return StringPin.internalDataStructure()(data)
