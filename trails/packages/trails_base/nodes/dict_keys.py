"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core import NodeBase
from trails.core.node_base import NodePinsSuggestionsHelper
from trails.core.common import StructureType, PinOptions


class dictKeys(NodeBase):
    def __init__(self, name):
        super(dictKeys, self).__init__(name)
        self.dict = self.createInputPin("dict", "AnyPin", structure=StructureType.Dict)
        self.dict.enableOptions(PinOptions.DictSupported)
        self.dict.onPinConnected.connect(self.dictConnected)
        self.dict.dictChanged.connect(self.dictChanged)
        self.keys = self.createOutputPin("keys", "AnyPin", structure=StructureType.Array)
        self.keys.disableOptions(PinOptions.ChangeTypeOnConnection)

    def dictConnected(self, other):
        self.keys.enableOptions(PinOptions.ChangeTypeOnConnection)
        self.keys.initType(other._data.keyType, True)
        self.keys.disableOptions(PinOptions.ChangeTypeOnConnection)

    def dictChanged(self, dataType):
        self.keys.enableOptions(PinOptions.ChangeTypeOnConnection)
        self.keys.initType(dataType, True)
        self.keys.disableOptions(PinOptions.ChangeTypeOnConnection)

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('AnyPin')
        helper.addOutputDataType('AnyPin')
        helper.addInputStruct(StructureType.Dict)
        helper.addOutputStruct(StructureType.Array)
        return helper

    @staticmethod
    def category():
        return 'Dictionary'

    @staticmethod
    def keywords():
        return ["keys"]

    @staticmethod
    def description():
        return 'Returns an array of dict keys.'

    def compute(self, *args, **kwargs):
        self.keys.setData(list(self.dict.getData().keys()))
