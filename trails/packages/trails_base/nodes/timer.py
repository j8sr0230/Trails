"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core import NodeBase
from trails.core.node_base import NodePinsSuggestionsHelper
from trails.core.common import StructureType
from trails.packages.trails_base.nodes import FLOW_CONTROL_COLOR


## Timer node
class timer(NodeBase):
    def __init__(self, name):
        super(timer, self).__init__(name)
        self.out = self.createOutputPin("OUT", 'ExecPin')
        self.beginPin = self.createInputPin("Begin", 'ExecPin', None, self.start)
        self.stopPin = self.createInputPin("Stop", 'ExecPin', None, self.stop)
        self.interval = self.createInputPin("Delta(s)", 'FloatPin')
        self.interval.setDefaultValue(0.2)
        self.accum = 0.0
        self.bWorking = False
        self.headerColor = FLOW_CONTROL_COLOR

    def Tick(self, delta):
        super(timer, self).Tick(delta)
        if self.bWorking:
            interval = self.interval.getData()
            if interval < 0.02:
                interval = 0.02
            self.accum += delta
            if self.accum >= interval:
                self.out.call()
                self.accum = 0.0

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('ExecPin')
        helper.addInputDataType('FloatPin')
        helper.addOutputDataType('ExecPin')
        helper.addInputStruct(StructureType.Single)
        helper.addOutputStruct(StructureType.Single)
        return helper

    def stop(self, *args, **kwargs):
        self.bWorking = False
        self.accum = 0.0

    def start(self, *args, **kwargs):
        self.accum = 0.0
        self.bWorking = True

    @staticmethod
    def category():
        return 'FlowControl'
