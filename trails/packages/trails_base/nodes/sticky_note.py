"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core import NodeBase


class stickyNote(NodeBase):
    def __init__(self, name):
        super(stickyNote, self).__init__(name)

    @staticmethod
    def category():
        return 'UI'

    @staticmethod
    def keywords():
        return []

    @staticmethod
    def description():
        return 'Sticky Note to save info with the graph'
