"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core import NodeBase
from trails.core.paths_registry import PathsRegistry
from trails.core.node_base import NodePinsSuggestionsHelper
from trails.core.common import StructureType
from trails.packages.trails_base.nodes import FLOW_CONTROL_ORANGE


class whileLoopBegin(NodeBase):
    def __init__(self, name):
        super(whileLoopBegin, self).__init__(name)
        self.lastCondition = False
        self.inExec = self.createInputPin('inExec', 'ExecPin', None, self.compute)
        self.condition = self.createInputPin('Condition', 'BoolPin')
        self.loopEndNode = self.createInputPin('Paired block', 'StringPin')
        self.loopEndNode.setInputWidgetVariant("ObjectPathWIdget")

        self.loopBody = self.createOutputPin('LoopBody', 'ExecPin')
        self.headerColor = FLOW_CONTROL_ORANGE
        self.setExperimental()

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('ExecPin')
        helper.addInputDataType('BoolPin')
        helper.addOutputDataType('ExecPin')
        helper.addInputStruct(StructureType.Single)
        helper.addOutputStruct(StructureType.Single)
        return helper

    @staticmethod
    def category():
        return 'FlowControl'

    @staticmethod
    def keywords():
        return ['iter']

    @staticmethod
    def description():
        return 'While loop begin block'

    def onNext(self, *args, **kwargs):
        currentCondition = self.condition.getData()
        if currentCondition:
            self.loopBody.call(*args, **kwargs)
        if self.lastCondition is True and currentCondition is False:
            endNodePath = self.loopEndNode.getData()
            loopEndNode = PathsRegistry().getEntity(endNodePath)
            loopEndNode.completed.call()
            self.lastCondition = False
            return
        self.lastCondition = currentCondition

    def compute(self, *args, **kwargs):
        endNodePath = self.loopEndNode.getData()
        loopEndNode = PathsRegistry().getEntity(endNodePath)
        if loopEndNode is not None:
            if loopEndNode.loopBeginNode.getData() != self.path():
                self.setError("Invalid pair")
                return
            if self.graph() is not loopEndNode.graph():
                err = "block ends in different graphs"
                self.setError(err)
                loopEndNode.setError(err)
                return
        else:
            self.setError("Pair {} not found".format(endNodePath))
            return

        self.onNext(*args, **kwargs)
