"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core import NodeBase
from trails.core.node_base import NodePinsSuggestionsHelper
from trails.core.common import StructureType
import ast


class stringToArray(NodeBase):
    def __init__(self, name):
        super(stringToArray, self).__init__(name)
        self.arrayData = self.createInputPin('data', 'StringPin', structure=StructureType.Single)
        self.outArray = self.createOutputPin('out', 'AnyPin', structure=StructureType.Array)
        self.result = self.createOutputPin('result', 'BoolPin')

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('StringPin')
        helper.addOutputDataType('AnyPin')
        helper.addOutputDataType('BoolPin')
        helper.addInputStruct(StructureType.Single)
        helper.addOutputStruct(StructureType.Array)
        helper.addOutputStruct(StructureType.Single)
        return helper

    @staticmethod
    def category():
        return 'GenericTypes'

    @staticmethod
    def keywords():
        return []

    @staticmethod
    def description():
        return 'Creates a list from ast.literal_eval(data) and then converts to output DataType'

    def compute(self, *args, **kwargs):
        outArray = []
        stringData = "[%s]"%self.arrayData.getData()
        if self.outArray.dataType == "AnyPin":
            self.outArray.setData(outArray)
            self.result.setData(False)
        else:
            splited = ast.literal_eval(stringData)
            self.outArray.setData(splited)
            self.result.setData(True)
