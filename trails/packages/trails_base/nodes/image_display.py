"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core import NodeBase
from trails.core.node_base import NodePinsSuggestionsHelper
from trails.core.common import StructureType, DEFAULT_IN_EXEC_NAME, DEFAULT_OUT_EXEC_NAME
from blinker import Signal
import os


class imageDisplay(NodeBase):
    def __init__(self, name):
        super(imageDisplay, self).__init__(name)
        self.loadImage = Signal(str)
        self.inExec = self.createInputPin(DEFAULT_IN_EXEC_NAME, 'ExecPin', None, self.compute)
        self.entity = self.createInputPin('path', 'StringPin')
        self.outExec = self.createOutputPin(DEFAULT_OUT_EXEC_NAME, 'ExecPin', None)
        self.failed = self.createOutputPin("failed", 'ExecPin', None)

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('ExecPin')
        helper.addInputDataType('StringPin')
        helper.addInputStruct(StructureType.Single)
        return helper

    @staticmethod
    def category():
        return 'UI'

    @staticmethod
    def keywords():
        return ['image']

    @staticmethod
    def description():
        return "Loads image to node body. This is UI only node"

    def compute(self, *args, **kwargs):
        path = self.entity.getData()
        if os.path.exists(path):
            self.loadImage.send(path)
            self.outExec.call()
        else:
            self.failed.call()
