"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core import NodeBase
from trails.core.common import StructureType, DEFAULT_IN_EXEC_NAME
from trails.core.graph_manager import GraphManagerSingleton
from trails.core.node_base import NodePinsSuggestionsHelper
from trails.packages.trails_base.nodes import FLOW_CONTROL_COLOR


class cliexit(NodeBase):
    def __init__(self, name):
        super(cliexit, self).__init__(name)
        self.inp0 = self.createInputPin(DEFAULT_IN_EXEC_NAME, 'ExecPin', None, self.compute)

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('ExecPin')
        helper.addInputStruct(StructureType.Single)
        return helper

    @staticmethod
    def category():
        return 'CLI'

    @staticmethod
    def keywords():
        return []

    @staticmethod
    def description():
        return 'Stops cli program loop'

    def compute(self, *args, **kwargs):
        man = GraphManagerSingleton().get()
        man.terminationRequested = True
