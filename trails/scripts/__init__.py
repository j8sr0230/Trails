"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import argparse
import sys
import os
import json
import threading
import time

from trails.app import Trails
from trails import graphUiParser, INITIALIZE
from Qt.QtWidgets import QApplication
from trails.core.version import Version
from trails.core.graph_manager import GraphManagerSingleton


def getGraphArguments(data, parser):
    """Adds arguments to parser using graph input nodes

    :param data: Serialized graph
    :type data: json
    :param parser: ArgumentParser class instance
    :type parser: ArgumentParser
    """
    typeMapping = {
        "BoolPin": bool,
        "StringPin": str,
        "IntPin": int,
        "FloatPin": float
    }

    graphNode = None
    for node in data["nodes"]:
        if node["type"] == "graphInputs":
            graphNode = node
            break

    for outPin in graphNode["outputs"]:
        pinType = outPin["dataType"]
        if pinType != "ExecPin":
            parser.add_argument("--{0}".format(outPin["name"]), type=typeMapping[pinType])


def main():
    parser = argparse.ArgumentParser(description="Trails CLI")
    parser.add_argument("-m", "--mode", type=str, default="edit", choices=["edit", "run", "runui"])
    parser.add_argument("-f", "--filePath", type=str, default="untitled.pygraph")
    parser.add_argument("--version", action="version", version=str(Version()))
    parsedArguments, unknown = parser.parse_known_args(sys.argv[1:])

    filePath = parsedArguments.filePath

    if not filePath.endswith(".pygraph"):
        filePath += ".pygraph"

    if parsedArguments.mode == "edit":
        app = QApplication(sys.argv)

        instance = Trails.instance(software="standalone")
        if instance is not None:
            app.setActiveWindow(instance)
            instance.show()
            if os.path.exists(filePath):
                with open(filePath, 'r') as f:
                    data = json.load(f)
                    instance.loadFromData(data)
                    instance.currentFileName = filePath

            try:
                sys.exit(app.exec_())
            except Exception as e:
                print(e)

    if parsedArguments.mode == "run":
        data = None
        if not os.path.exists(filePath):
            print("No such file. {}".format(filePath))
            return
        with open(filePath, 'r') as f:
            data = json.load(f)
        getGraphArguments(data, parser)
        parsedArguments = parser.parse_args()

        # load updated data
        INITIALIZE()
        GM = GraphManagerSingleton().get()
        GM.deserialize(data)

        # fake main loop
        def programLoop():
            while True:
                GM.Tick(deltaTime=0.02)
                time.sleep(0.02)
                if GM.terminationRequested:
                    break

        # call graph inputs nodes
        root = GM.findRootGraph()
        graphInputNodes = root.getNodesList(classNameFilters=["graphInputs"])
        evalFunctions = []
        for graphInput in graphInputNodes:
            # update data
            for outPin in graphInput.outputs.values():
                if outPin.isExec():
                    evalFunctions.append(outPin.call)
                if hasattr(parsedArguments, outPin.name):
                    cliValue = getattr(parsedArguments, outPin.name)
                    if cliValue is not None:
                        outPin.setData(cliValue)

        for foo in evalFunctions:
            foo()

        loopThread = threading.Thread(target=programLoop)
        loopThread.start()
        loopThread.join()

    if parsedArguments.mode == "runui":
        graphUiParser.run(filePath)
