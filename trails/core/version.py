"""
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import re
from collections import namedtuple

from trails import __version__

# https://semver.org/#is-there-a-suggested-regular-expression-regex-to-check-a-semver-string
VERSION = re.compile('^(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)(?:-(?P<prerelease>(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$')


class Version(namedtuple("Version", ("major", "minor", "patch", "prerelease", "build"))):
    """Version class according to `semantic versioning <https://semver.org>`_

    Based on
    - https://packaging.python.org/en/latest/guides/single-sourcing-package-version/,
      option 4.
    - https://docs.python.org/3/library/collections.html#namedtuple-factory-function-for-tuples-with-named-fields,
      subclassing named tuple

    """

    def __new__(cls, *version, prerelease=None, build=None):
        if not version:
            # Use package version `__version__`
            major, minor, patch, prerelease, build = VERSION.match(__version__).groups()
        elif len(version) == 1 and isinstance(version[0], str):
            # A version string
            major, minor, patch, prerelease, build = VERSION.match(version[0]).groups()
        elif len(version) == 3:
            major, minor, patch = version
        else:
            raise TypeError(
                f'Either a version string or three numbers are required. '
                f'Got: {version}')

        # Convert to integer values
        err = {}
        try:
            major = int(major)
        except ValueError:
            err['major'] = major
        try:
            minor = int(minor)
        except ValueError:
            err['minor'] = minor
        try:
            patch = int(patch)
        except ValueError:
            err['patch'] = patch
        if err:
            err_msg = [f'{k}={v}' for k, v in err.items()]
            raise ValueError(
                f'The version numbers must be of type integer. '
                f'Got: {", ".join(err_msg)}')

        return namedtuple(
            "Version",
            ("major", "minor", "patch", "prerelease", "build")).__new__(
                cls, major, minor, patch, prerelease, build)

    def __str__(self):
        return (
            f"{self.major}.{self.minor}.{self.patch}"
            f"{f'-{self.prerelease}' if self.prerelease else ''}"
            f"{f'+{self.build}' if self.build else ''}")
