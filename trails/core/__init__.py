"""Core functionality of the Trails.

This module will be eventually moved to own repo.


Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core.pin_base import PinBase
from trails.core.node_base import NodeBase
from trails.core.graph_base import GraphBase
from trails.core.graph_manager import GraphManager
from trails.core.function_library import FunctionLibraryBase
from trails.core.function_library import IMPLEMENT_NODE
from trails.core import common
from trails.core import version
