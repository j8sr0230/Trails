"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import os
import sys
import json
import threading
import time

from Qt.QtWidgets import *
from Qt import QtGui
from Qt import QtCore
from trails import INITIALIZE
from trails.core.graph_manager import GraphManagerSingleton
from trails.ui.canvas.ui_node_base import getUINodeInstance
from trails.config_manager import UIThemeManager
from trails.ui.widgets.properties_framework import CollapsibleFormWidget
import trails.ui.resources


def run(filePath):
    app = QApplication(sys.argv)
    app.setStyle(QStyleFactory.create("plastique"))
    app.setStyleSheet(UIThemeManager().stylesheet)

    msg = QMessageBox()
    msg.setWindowIcon(QtGui.QIcon(":/logo.svg"))
    msg.setIcon(QMessageBox.Critical)

    if os.path.exists(filePath):
        with open(filePath, 'r') as f:
            data = json.load(f)

        # Window to display inputs
        prop = QDialog()
        prop.setLayout(QVBoxLayout())
        prop.setWindowTitle(filePath)
        prop.setWindowIcon(QtGui.QIcon(":/logo.svg"))
        # Initalize packages
        try:
            INITIALIZE()
            man = GraphManagerSingleton().get()
            man.deserialize(data)
            grph = man.findRootGraph()
            inputs = grph.getNodesByClassName("graphInputs")
            if len(inputs) > 0:
                for inp in inputs:
                    uiNode = getUINodeInstance(inp)
                    uiNodeJsonTemplate = inp.serialize()
                    uiNodeJsonTemplate["wrapper"] = inp.wrapperJsonData
                    uiNode.postCreate(uiNodeJsonTemplate)
                    cat = CollapsibleFormWidget(headName=inp.name)
                    prop.layout().addWidget(cat)
                    cat = uiNode.createOutputWidgets(cat)

                nodes = grph.getNodesList()
                if len(nodes) > 0:
                    for node in nodes:
                        uiNode = getUINodeInstance(node)
                        uiNodeJsonTemplate = node.serialize()
                        uiNodeJsonTemplate["wrapper"] = node.wrapperJsonData
                        uiNode.postCreate(uiNodeJsonTemplate)
                        if uiNode.bExposeInputsToCompound:
                            cat = CollapsibleFormWidget(headName="{} inputs".format(node.name))
                            prop.layout().addWidget(cat)
                            uiNode.createInputWidgets(cat, pins=False)
                prop.show()
                def programLoop():
                    while True:
                        man.Tick(deltaTime=0.02)
                        time.sleep(0.02)
                        if man.terminationRequested:
                            break
                t = threading.Thread(target=programLoop)
                t.start()

                def quitEvent():
                    man.terminationRequested = True
                    t.join()
                app.aboutToQuit.connect(quitEvent)
            # If no GraphInput Nodes Exit propgram
            else:
                msg.setInformativeText(filePath)
                msg.setDetailedText("The file doesn't containt graphInputs nodes")
                msg.setWindowTitle("Trails UI Graph Parser")
                msg.setStandardButtons(QMessageBox.Ok)
                msg.show()

        except Exception as e:
            msg.setText("Error reading Graph")
            msg.setInformativeText(filePath)
            msg.setDetailedText(str(e))
            msg.setWindowTitle("Trails UI Graph Parser")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.show()

    else:
        msg.setText("File Not Found")
        msg.setInformativeText(filePath)
        msg.setWindowTitle("Trails UI Graph Parser")
        msg.setStandardButtons(QMessageBox.Ok)
        msg.show()

    try:
        sys.exit(app.exec_())
    except Exception as e:
        print(e)
