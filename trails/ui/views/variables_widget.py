"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import json
from types import MethodType
import uuid

from Qt import QtCore, QtGui
from Qt.QtWidgets import (
    QListWidget,
    QListWidgetItem,
    QWidget,
    QAbstractItemView
)

from trails.ui.editor_history import EditorHistory
from trails.ui.canvas.ui_variable import UIVariable
from trails.ui.views.variables_widget_ui import Ui_Form
from trails.ui.canvas.ui_common import clearLayout
from trails.core.common import AccessLevel

VARIABLE_TAG = "VAR"
VARIABLE_DATA_TAG = "VAR_DATA"


class VariablesListWidget(QListWidget):
    """docstring for VariablesListWidget."""
    def __init__(self, parent=None):
        super(VariablesListWidget, self).__init__(parent)
        self.setDragDropMode(QAbstractItemView.InternalMove)
        self.setDefaultDropAction(QtCore.Qt.MoveAction)
        self.setSelectionMode(QAbstractItemView.SingleSelection)
        self.setSelectionRectVisible(True)

    def mousePressEvent(self, event):
        super(VariablesListWidget, self).mousePressEvent(event)
        w = self.itemWidget(self.currentItem())
        if w:
            drag = QtGui.QDrag(self)
            mime_data = QtCore.QMimeData()
            varJson = w.serialize()
            dataJson = {VARIABLE_TAG: True, VARIABLE_DATA_TAG: varJson}
            mime_data.setText(json.dumps(dataJson))
            drag.setMimeData(mime_data)
            drag.exec_()


class VariablesWidget(QWidget, Ui_Form):
    """docstring for VariablesWidget"""

    def __init__(self, trailsInstance, parent=None):
        super(VariablesWidget, self).__init__(parent)
        self.setupUi(self)
        self.trailsInstance = trailsInstance
        self.trailsInstance.graphManager.get().graphChanged.connect(self.onGraphChanged)
        self.pbNewVar.clicked.connect(self.createVariable)
        self.listWidget = VariablesListWidget()
        self.lytListWidget.addWidget(self.listWidget)
        self.trailsInstance.newFileExecuted.connect(self.actualize)

    def actualize(self):
        self.clear()
        # populate current graph
        graph = self.trailsInstance.graphManager.get().activeGraph()
        if graph:
            for var in graph.getVarList():
                self.createVariableWrapperAndAddToList(var)

    def onGraphChanged(self, *args, **kwargs):
        self.actualize()

    def clear(self):
        """Does not removes any variable. UI only
        """
        self.listWidget.clear()

    def killVar(self, uiVariableWidget):
        variableGraph = uiVariableWidget._rawVariable.graph
        variableGraph.killVariable(uiVariableWidget._rawVariable)
        self.actualize()

        self.clearProperties()
        EditorHistory().saveState("Kill variable", modify=True)

    def createVariableWrapperAndAddToList(self, rawVariable):
        uiVariable = UIVariable(rawVariable, self)
        item = QListWidgetItem(self.listWidget)
        item.setSizeHint(QtCore.QSize(60, 20))
        self.listWidget.setItemWidget(item, uiVariable)
        return uiVariable

    def createVariable(self, dataType=str('BoolPin'), accessLevel=AccessLevel.public, uid=None):
        rawVariable = self.trailsInstance.graphManager.get().activeGraph().createVariable(dataType=dataType, accessLevel=accessLevel, uid=uid)
        uiVariable = self.createVariableWrapperAndAddToList(rawVariable)
        EditorHistory().saveState("Create variable", modify=True)
        return uiVariable

    def clearProperties(self):
        self.trailsInstance.onRequestClearProperties()

    def onUpdatePropertyView(self, uiVariable):
        self.trailsInstance.onRequestFillProperties(uiVariable.createPropertiesWidget)
