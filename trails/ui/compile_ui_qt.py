"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import os
import pyside2uic
import subprocess


CURRENT_DIR = os.path.dirname(__file__).replace('\\', '/') + '/'
INTERPRETER_PATH = 'python.exe'


def ui_to_py(ui_file):
    if not os.path.isfile(ui_file):
        msg = 'no such file'
        print(msg)
        return msg
    py_file_name = os.path.splitext(ui_file)[0] + '.py'
    with open(py_file_name, 'w') as py_file:
        try:
            pyside2uic.compileUi(ui_file, py_file)
            print('{0} converted to {1}.'.format(ui_file.upper(), py_file_name.upper()))
        except Exception as e:
            print('Error: compilation error.', e)

    bakFileName = py_file_name.replace(".py", "_backup.py")

    # convert to cross compatible code
    subprocess.call([INTERPRETER_PATH, '-m', 'Qt', '--convert', py_file_name])

    if(os.path.isfile(bakFileName)):
        os.remove(bakFileName)
        print("REMOVING", bakFileName)


def compile():
    for d, dirs, files in os.walk(CURRENT_DIR):
        if "Python" in d or ".git" in d:
            continue
        for f in files:
            if "." in f:
                ext = f.split('.')[1]
                if ext == 'ui':
                    uiFile = os.path.join(d, f)
                    ui_to_py(uiFile)


if __name__ == '__main__':
    compile()
