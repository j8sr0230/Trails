"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from collections import defaultdict
__REGISTERED_TOOLS = defaultdict(list)


def REGISTER_TOOL(packageName, toolClass):
    registeredToolNames = [tool.name() for tool in __REGISTERED_TOOLS[packageName]]
    if toolClass.name() not in registeredToolNames:
        __REGISTERED_TOOLS[packageName].append(toolClass)
        toolClass.packageName = packageName


def GET_TOOLS():
    return __REGISTERED_TOOLS
