"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt import QtGui
from Qt import QtCore
from trails.ui.canvas.loop_back_drop import backDrop
from trails.core.paths_registry import PathsRegistry
from trails.ui.utils.convex_hull import convex_hull
from trails.ui.canvas.painters import ConnectionPainter

class IConvexHullBackDrop(object):
    """Convex hull backdrop routines. Used by for loop and while loop nodes"""

    def __init__(self):
        super(IConvexHullBackDrop, self).__init__()
        self.poly = None
        self.left = 0
        self.top = 0
        self.right = 0
        self.down = 0
        self.convex_hull = []
        self.backDrop = backDrop(self)

    def computeHull(self):

        loopEndNodePath = self.getPinSG("Paired block").getData()
        loopEndNode = PathsRegistry().getEntity(loopEndNodePath)

        if loopEndNode is None:
            self.poly = QtGui.QPainterPath()
            return
        if self.isUnderCollapsedComment():
            p = [self.getTopMostOwningCollapsedComment()]
        else:
            p = [self]
        if loopEndNode.__class__.__name__ == "loopEnd" and loopEndNode.getWrapper() is not None:
            uiLoopEnd = loopEndNode.getWrapper()
            if loopEndNode.isUnderActiveGraph():
                if uiLoopEnd.isUnderCollapsedComment():
                    p.append(uiLoopEnd.getTopMostOwningCollapsedComment())
                else:
                    p.append(uiLoopEnd)

        else:
            self.poly = QtGui.QPainterPath()
            return

        p += self.getBetwenLoopNodes(self)

        path = []
        self.left = 0
        self.top = 0
        self.right = 0
        self.down = 0
        for i in p:
            relPos = i.scenePos()
            self.left = min(self.left, relPos.x())
            self.top = max(self.top, relPos.y())
            self.right = max(self.right, relPos.x())
            self.down = min(self.down, relPos.y())
            relSize = QtCore.QPointF(i.getNodeWidth(), i.geometry().height())
            path.append((relPos.x() - 5, relPos.y() - 5))
            path.append((relPos.x() + relSize.x() + 5, relPos.y() - 5))
            path.append((relPos.x() + relSize.x() + 5, relPos.y() + relSize.y() + 5))
            path.append((relPos.x() - 5, relPos.y() + relSize.y() + 5))

        if len(path) >= 3:
            self.convex_hull = convex_hull(path)
            path = []
            for i in self.convex_hull:
                path.append(QtCore.QPointF(i[0], i[1]))
            self.poly,none = ConnectionPainter.roundCornersPath(path,6,True)
