"""
Copyright (C) 2015-2019  ?? Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt import QtCore
from Qt import QtGui

from Qt.QtWidgets import *

from trails.config_manager import ShortcutsManager, GraphThemeManager


class WatchItem(QGraphicsTextItem):
    """docstring for WatchItem."""
    def __init__(self, text=""):
        super(WatchItem, self).__init__(text)
        self.setDefaultTextColor(GraphThemeManager().PinWatch)
        font = QtGui.QFont("Consolas")
        font.setPointSize(6)
        self.setFont(font)

    def paint(self, painter, option, widget):
        painter.drawRect(self.boundingRect())
        painter.fillRect(self.boundingRect(), GraphThemeManager().PinWatchBackground)
        super(WatchItem, self).paint(painter, option, widget)
