"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt import QtGui
from Qt import QtCore
from Qt.QtWidgets import QGraphicsWidget


class backDrop(QGraphicsWidget):
    def __init__(self, parent):
        super(backDrop, self).__init__()
        self.parent = parent
        self.rect = QtCore.QRectF()
        self.parent._rawNode.killed.connect(self.parentNodeKilled)

    def parentNodeKilled(self, *args):
        scene = self.scene()
        if scene is not None:
            scene.removeItem(self)
            del self

    def boundingRect(self):
        try:
            return QtCore.QRectF(QtCore.QPointF(self.parent.left - 5, self.parent.top + 5), QtCore.QPointF(self.parent.right + 5, self.parent.down - 5))
        except:
            return QtCore.QRectF(0, 0, 0, 0)

    def paint(self, painter, option, widget):
        if not self.parent.isUnderActiveGraph():
            return

        roundRectPath = QtGui.QPainterPath()
        self.parent.computeHull()
        if self.parent.poly is not None:
            color = QtGui.QColor(self.parent.headColorOverride)
            color.setAlpha(50)
            pen = QtGui.QPen(self.parent.headColorOverride, 0.5)
            painter.setPen(pen)
            painter.fillPath(self.parent.poly, color)
            painter.drawPath(self.parent.poly)
