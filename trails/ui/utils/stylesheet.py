"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import enum

import step

from trails.core.common import SingletonDecorator
from trails.config_manager import UIThemeManager


class ConnectionTypes(enum.IntEnum):
    Cubic = 0
    Circuit = 1
    ComplexCircuit = 2
    Linear = 3


@SingletonDecorator
class sliderStyleSheet:

    @property
    def MainColor(self):
        return UIThemeManager().MainColor

    @property
    def MainColorLighter(self):
        return UIThemeManager().MainColorLighter

    def getSliderStyleSheet(self, name):
        Styles = {
            "sliderStyleSheetA":
                """
                QWidget{
                    border: 1.25 solid black;
                }
                QSlider::groove:horizontal,
                QSlider::sub-page:horizontal {
                    %if (MainColor != "None"):
                    background: {{MainColor}};
                    %endif
                }
                QSlider::add-page:horizontal,
                QSlider::sub-page:horizontal:disabled {
                    background: rgb(32, 32, 32);
                }
                QSlider::add-page:horizontal:disabled {
                    background: grey;
                }
                QSlider::handle:horizontal {
                    width: 1px;
                }
                """,
            "sliderStyleSheetB":
                """
                QSlider::groove:horizontal {
                    border: 1px solid #bbb;
                    background: white;
                    height: 3px;
                    border-radius: 2px;
                }
                QSlider::sub-page:horizontal {
                    %if (MainColor != "None"):
                    background: %s;
                    %endif
                    border: 0px solid #777;
                    height: 3px;
                    border-radius: 2px;
                }
                QSlider::add-page:horizontal {
                    background: #fff;
                    border: 1px solid #777;
                    height: 3px;
                    border-radius: 2px;
                }
                QSlider::handle:horizontal {
                    background: qlineargradient(x1:0, y1:0, x2:1, y2:1,
                        stop:0 #eee, stop:1 #ccc);
                    border: 1px solid #777;
                    width: 4px;
                    margin-top: -8px;
                    margin-bottom: -8px;
                    border-radius: 2px;
                    height : 10px;
                }
                QSlider::handle:horizontal:hover {
                    background: qlineargradient(x1:0, y1:0, x2:1, y2:1,
                        stop:0 #fff, stop:1 #ddd);
                    border: 1px solid #444;
                    border-radius: 2px;
                }
                QSlider::sub-page:horizontal:disabled {
                    background: #bbb;
                    border-color: #999;
                }
                QSlider::add-page:horizontal:disabled {
                    background: #eee;
                    border-color: #999;
                }
                QSlider::handle:horizontal:disabled {
                    background: #eee;
                    border: 1px solid #aaa;
                    border-radius: 2px;
                    height : 10;
                }
                """,
            "sliderStyleSheetC":
                """
                QSlider,QSlider:disabled,QSlider:focus {
                    background: qcolor(0,0,0,0);   }

                QSlider::groove:horizontal {
                    border: 1px solid #999999;
                    background: qcolor(0,0,0,0);
                 }
                QSlider::handle:horizontal {
                    background:  rgba(255, 255, 255, 150);
                    width: 10px;
                    border-radius: 4px;
                    border: 1.5px solid black;
                 }
                 QSlider::handle:horizontal:hover {
                    border: 2.25px solid;
                    %if (MainColor != "None"):
                    border: {{MainColor}};
                    %endif
                 }
                """,
            "draggerstyleSheet":
                """
                QGroupBox{
                    border: 0.5 solid darkgrey;
                    background : black;
                    color: white;
                }
                QLabel{
                    background: transparent;
                    border: 0 solid transparent;
                    color: white;
                }
                """,
            "draggerstyleSheetHover":
                """
                QGroupBox{
                    border: 0.5 solid darkgrey;
                    %if (MainColor != "None"):
                    background : {{MainColor}};
                    %endif
                    color: white;
                }
                QLabel{
                    background: transparent;
                    border: 0 solid transparent;
                    color: white;
                }
                """,
            "timeStyleSheet":
                """
                QSlider,QSlider:disabled,QSlider:focus{
                    background: qcolor(0,0,0,0);   }
                 QSlider::groove:horizontal {
                    border: 1px solid #999999;
                    background: qcolor(0,0,0,0);
                 }
                QSlider::handle:horizontal {
                    %if (MainColor != "None"):
                    background: {{MainColor}};
                    %endif
                    width: 3px;
                 }
                """,
        }
        return step.Template(Styles[name]).expand(
            {"MainColor": UIThemeManager().getCssString("MainColor")})
