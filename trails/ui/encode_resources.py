"""
Copyright 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import os
from pathlib import Path
import platform
import subprocess
from Qt import __binding__


RESOURCESDIR = './resources'
RESOURCEFILE = 'resources.qrc'


if __binding__ == 'PySide2':
    compiler = 'pyside2-rcc'
elif __binding__ == 'PySide':
    compiler = 'pyside-rcc'
elif __binding__ == 'PyQt4':
    compiler = 'pyrcc4'
elif __binding__ == 'PyQt5':
    compiler = 'pyrcc5'

system = platform.system()
if system == 'Windows':
    binding = __import__(__binding__)
    path = Path(binding.__file__).parent
    compiler = (path / compiler).with_suffix('.exe')


def main():
    print('Encoding : Resources')
    filepath = Path(RESOURCESDIR).resolve()
    resourceFile = Path(RESOURCEFILE)

    with open(resourceFile, 'w') as outf:
        outf.write('<RCC>\n  <qresource>\n')
        for root, dirs, files in os.walk(filepath):
            for file in files:
                dirname = Path(root, file).relative_to(filepath.parent)
                if dirname.suffix != '.qrc':
                    write = '     <file alias="%s">%s</file>\n' % (
                        file, dirname)
                    outf.write(write)
        outf.write('  </qresource>\n</RCC>')
    args = [compiler, '-compress', '2', '-threshold', '3', '-o',
            os.path.join(os.path.dirname(filepath), r'resources.py'), resourceFile]
    p = subprocess.Popen(
        args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    if out:
        print('OUTPUT: %s' % out.decode())
    if err:
        print('ERRORS: %s' % err.decode())
    else:
        print('Compiled : Resources')


if __name__ == '__main__':
    main()
