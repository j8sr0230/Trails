"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt.QtWidgets import *
from Qt import QtCore, QtGui

from trails.config_manager import PreferencesManager, ShortcutsManager
from trails.core.common import SingletonDecorator


class CategoryButton(QPushButton):
    """docstring for CategoryButton."""
    def __init__(self, icon=None, text="test", parent=None):
        super(CategoryButton, self).__init__(text, parent)
        self.setMinimumHeight(30)
        self.setCheckable(True)
        self.setAutoExclusive(True)


class CategoryWidgetBase(QScrollArea):
    """docstring for CategoryWidgetBase."""

    manager = None
    section = None
    DEFAULT_PREFERENCES = {}

    def __init__(self, parent=None):
        super(CategoryWidgetBase, self).__init__(parent)
        self.setWidgetResizable(True)

    def serialize(self):
        # Should be implemented by the subclass
        raise NotImplementedError(
            "Should be implemented by the subclass")

    def update_manager(self):
        self.manager[self.section].overwrite_all(**self.serialize())
        return self.manager

    def onShow(self, settings):
        pass


@SingletonDecorator
class PreferencesWindow(QMainWindow):
    """docstring for PreferencesWindow."""
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        self.resize(600, 400)
        self.setWindowTitle("Preferences")
        self.centralWidget = QWidget(self)
        self.centralWidget.setObjectName("centralWidget")
        self.verticalLayout = QVBoxLayout(self.centralWidget)
        self.verticalLayout.setContentsMargins(1, 1, 1, 1)
        self.verticalLayout.setObjectName("verticalLayout")
        self.splitter = QSplitter(self.centralWidget)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName("splitter")
        self.scrollArea = QScrollArea(self.splitter)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 497, 596))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.scrollArea.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Preferred)
        self.verticalLayout_3 = QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout_3.setContentsMargins(1, 1, 1, 1)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.verticalLayout_3.setSpacing(2)
        self.categoriesVerticalLayout = QVBoxLayout()
        self.categoriesVerticalLayout.setObjectName("categoriesLayout")
        spacer = QSpacerItem(10, 10, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.categoriesVerticalLayout.addItem(spacer)
        self.verticalLayout_3.addLayout(self.categoriesVerticalLayout)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.scrollArea.setMinimumWidth(200)
        self.stackedWidget = QStackedWidget(self.splitter)
        self.stackedWidget.setObjectName("stackedWidget")
        self.stackedWidget.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.verticalLayout.addWidget(self.splitter)
        self.setCentralWidget(self.centralWidget)
        self.splitter.setSizes([150, 450])
        self._indexes = {}
        self.categoryButtons = {}
        self.buttonsLay = QHBoxLayout()
        pbClosePrefs = QPushButton("Close")
        pbClosePrefs.setToolTip("Close preferences dialog")
        pbClosePrefs.clicked.connect(self.closePreferences)
        pbSavePrefs = QPushButton("Save")
        pbSavePrefs.setToolTip("Save preferences")
        pbSavePrefs.clicked.connect(self.savePreferences)
        self.buttonsLay.addWidget(pbClosePrefs)
        self.buttonsLay.addWidget(pbSavePrefs)
        self.verticalLayout.addLayout(self.buttonsLay)

    def selectByName(self, name):
        if name in self._indexes:
            index = self._indexes[name][0]
            self.stackedWidget.setCurrentIndex(index)
            self.categoryButtons[index].setChecked(True)

    def showEvent(self, event):
        for name, indexWidget in self._indexes.items():
            index, widget = indexWidget
            widget.onShow()

    def savePreferences(self):
        managers = {}
        for name, indexWidget in self._indexes.items():
            index, widget = indexWidget
            manager = widget.update_manager()
            managers[manager.__class__.__name__] = manager
        # Only save each manager once
        for manager in managers.values():
            manager.save()
        self.close()

    def closePreferences(self):
        self.close()

    def addCategory(self, name, widget):
        categoryButton = CategoryButton(text=name)
        self.categoriesVerticalLayout.insertWidget(self.categoriesVerticalLayout.count() - 1, categoryButton)
        widget.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        index = self.stackedWidget.addWidget(widget)
        self._indexes[name] = (index, widget)
        self.categoryButtons[index] = categoryButton
        categoryButton.clicked.connect(lambda checked=False, idx=index: self.switchCategoryContent(idx))

    def switchCategoryContent(self, index):
        self.stackedWidget.setCurrentIndex(index)
        self.categoryButtons[index].toggle()
