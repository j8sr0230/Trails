"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt.QtWidgets import *
from Qt import QtCore, QtGui


class KeyCaptureWidget(QPushButton):
    """docstring for KeyCaptureWidget."""
    captured = QtCore.Signal(object)

    def __init__(self, parent=None):
        super(KeyCaptureWidget, self).__init__(parent)
        self.bCapturing = False
        self._currentKey = None
        self.setText("NoKey")
        self.setCheckable(True)
        self.setToolTip("""
            <b>Left click</b> to start/stop capturing:<br/>
            &nbsp;&nbsp;&nbsp;&nbsp;The key will be accepted immediately..<br/>
            &nbsp;&nbsp;&nbsp;&nbsp;Modifiers will not be accepted.<br/>
            <b>Right click</b> to clear.""")

        self.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        self.actionClear = QAction("Clear", None)
        self.actionClear.triggered.connect(self.clearKey)
        self.addAction(self.actionClear)

    def clearKey(self):
        self.setChecked(False)
        self.bCapturing = False
        self.currentKey = None

    @property
    def currentKey(self):
        return self._currentKey

    @currentKey.setter
    def currentKey(self, value):
        if value is None:
            self.setText("NoKey")
            self.bCapturing = False
            self.setChecked(False)
        else:
            self._currentKey = value
            self.setText(QtGui.QKeySequence(self._currentKey).toString())
            self.bCapturing = False
            self.setChecked(False)
            self.captured.emit(self._currentKey)

    def mousePressEvent(self, event):
        super(KeyCaptureWidget, self).mousePressEvent(event)
        if event.button() == QtCore.Qt.MouseButton.LeftButton:
            if not self.bCapturing:
                self.bCapturing = True
                self.setText("capturing…")
            else:
                self.bCapturing = False
                self.setText(QtGui.QKeySequence(self._currentKey).toString())

    def keyPressEvent(self, event):
        super(KeyCaptureWidget, self).keyPressEvent(event)
        key = event.key()
        modifiers = event.modifiers()
        if modifiers == QtCore.Qt.NoModifier:
            self.currentKey = QtCore.Qt.Key(key)
        if not modifiers == QtCore.Qt.NoModifier:
            self.resetToDefault()


if __name__ == "__main__":
    import sys
    a = QApplication(sys.argv)

    w = KeyCaptureWidget()
    w.show()

    sys.exit(a.exec_())
