"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt.QtWidgets import QMenu
from Qt.QtWidgets import QAction


class ContextMenuGenerator(object):
    """docstring for ContextMenuGenerator."""
    def __init__(self, menuDataBuilder):
        super(ContextMenuGenerator, self).__init__()
        self.builder = menuDataBuilder

    def __createMenuEntry(self, parentMenu, menuEntryData):
        if "separator" in menuEntryData:
            parentMenu.addSeparator()
            return
        icon = menuEntryData['icon']
        if "sub_menu" in menuEntryData:
            subMenuData = menuEntryData["sub_menu"]
            subMenu = parentMenu.addMenu(menuEntryData["title"])
            if icon is not None:
                subMenu.setIcon(icon)
            self.__createMenuEntry(subMenu, subMenuData)
        else:
            action = parentMenu.addAction(menuEntryData['title'])
            if icon is not None:
                action.setIcon(icon)
            action.triggered.connect(menuEntryData['callback'])

    def generate(self):
        menuData = self.builder.get()
        menu = QMenu()
        for menuEntry in menuData:
            self.__createMenuEntry(menu, menuEntry)
        return menu
