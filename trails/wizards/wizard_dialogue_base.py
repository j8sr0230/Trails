"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt import QtCore
from Qt import QtGui
from Qt.QtWidgets import *


class WizardDialogueBase(QDialog):
    """docstring for WizardDialogueBase."""
    def __init__(self, parent=None):
        super(WizardDialogueBase, self).__init__(parent)
        self.setWindowTitle("Package wizard")
        self.setWindowIcon(QtGui.QIcon(":/logo.png"))
        self.resize(700, 500)
        self.mainLayout = QVBoxLayout(self)
        self.mainLayout.setObjectName("mainLayout")
        self.mainLayout.setSpacing(1)
        self.mainLayout.setContentsMargins(1, 1, 1, 1)
        self.stackWidget = QStackedWidget()
        self.stackWidget.currentChanged.connect(self.updateMessage)

        # message section
        self.messageLayout = QHBoxLayout()
        self.messageWidget = QLabel()
        self.messageWidget.setTextFormat(QtCore.Qt.RichText)
        self.messageWidget.setWordWrap(True)
        self.messageWidget.setAlignment(QtCore.Qt.AlignCenter)
        wizardImage = QLabel("test")
        wizardImage.setPixmap(QtGui.QPixmap(":/wizard-cat.png").scaled(125, 125))
        wizardImage.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
        self.messageLayout.addWidget(self.messageWidget)
        self.messageLayout.addWidget(wizardImage)
        self.mainLayout.addLayout(self.messageLayout)

        # add user input section
        # ...
        self.messages = {}
        self.titles = {}
        self.notes = {}
        self.pageValidationHooks = {}
        self.errorMessages = {}
        self.pageEnterCallbacks = {}
        self.addGreetPage()
        self.populate()
        self.addFinalPage()

        self.mainLayout.addWidget(self.stackWidget)

        self.progress = QProgressBar()
#        self.progress.setTextVisible(False)
        self.progress.setObjectName("progress")
        self.progress.setRange(0, 100)
        self.mainLayout.addWidget(self.progress)

        # add navigation buttons
        # ...
        self.navigationLayout = QHBoxLayout()
        self.navigationLayout.setObjectName("navigationLayout")
        self.navigationLayout.setContentsMargins(5, 1, 5, 5)
        self.goBackButton = QPushButton("Go back")
        self.goBackButton.clicked.connect(self.onGoBack)
        self.goForwardButton = QPushButton("Go forward")
        self.goForwardButton.clicked.connect(self.onGoForward)
        self.cancelButton = QPushButton("Cancel")
        self.cancelButton.clicked.connect(self.onCancel)
        self.navigationLayout.addWidget(self.cancelButton)
        spacerItem = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Maximum)
        self.navigationLayout.addItem(spacerItem)
        self.navigationLayout.addWidget(self.goBackButton)
        self.navigationLayout.addWidget(self.goForwardButton)
        self.mainLayout.addLayout(self.navigationLayout)

        self.updateNavigationVisibility()
        self.updateMessage(0)

    def updateMessage(self, pageNum, error=False):
        widget = self.stackWidget.widget(pageNum)
        if widget in self.messages:
            msg = f'<big>{self.messages[widget]}</big>'
            if self.titles[widget]:
                msg = f'<big><strong>{self.titles[widget]}</strong></big><br/><br/>{msg}'
            if self.notes[widget]:
                msg = f'{msg}<br/><br/><strong>Note:</strong> {self.notes[widget]}'
            if error:
                msg = f'{msg}<br/><br/><big><strong style="color:red;">Error:</strong></big> {self.errorMessages[widget]}'
            self.messageWidget.setText(msg)

    def addGreetPage(self):
        self.addPageWidget(QWidget(), "Let's create some new stuff.", titleHtml="Hello!")

    def addFinalPage(self):
        self.addPageWidget(QWidget(), "Have fun!")

    def updateProgress(self):
        numStates = self.stackWidget.count() - 1
        chunk = 100 / numStates
        self.progress.setValue(chunk * self.stackWidget.currentIndex())

    def updateNavigationVisibility(self):
        self.goBackButton.show()
        self.goForwardButton.show()
        self.goForwardButton.setText("Go forward")
        if self.stackWidget.currentIndex() == 0:
            self.goBackButton.hide()
        if self.stackWidget.currentIndex() == self.stackWidget.count() - 1:
            self.goForwardButton.setText("Finish")
        self.updateProgress()

    def onGoBack(self):
        futureIndex = self.stackWidget.currentIndex() - 1
        self.stackWidget.setCurrentIndex(futureIndex)
        self.updateMessage(futureIndex)
        self.updateNavigationVisibility()

    def onDone(self):
        self.accept()

    def onGoForward(self):
        futureIndex = self.stackWidget.currentIndex() + 1
        isCurrentPageValid = self.pageValidationHooks[self.stackWidget.currentWidget()]()
        if isCurrentPageValid:
            self.stackWidget.setCurrentIndex(futureIndex)
            self.updateNavigationVisibility()
            self.pageValidationHooks[self.stackWidget.currentWidget()]()
            self.pageEnterCallbacks[self.stackWidget.currentWidget()]()
            if futureIndex == self.stackWidget.count():
                self.onDone()
        else:
            self.updateMessage(self.stackWidget.currentIndex(), error=True)

    def onCancel(self):
        self.reject()

    def addPageWidget(self, widget, messageHtml, titleHtml="", noteHtml="", errorMsgHtml="", validationHook=lambda: True, pageEnterCallback=lambda: None):
        self.stackWidget.addWidget(widget)
        self.messages[widget] = messageHtml
        self.titles[widget] = titleHtml
        self.notes[widget] = noteHtml
        self.pageValidationHooks[widget] = validationHook
        self.errorMessages[widget] = errorMsgHtml
        self.pageEnterCallbacks[widget] = pageEnterCallback

    def populate(self):
        pass

    @staticmethod
    def run():
        instance = WizardDialogueBase()
        instance.exec_()


if __name__ == "__main__":
    import sys
    app = QApplication(sys.argv)

    w = WizardDialogueBase()
    w.show()

    sys.exit(app.exec_())
