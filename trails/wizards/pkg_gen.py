"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import os
import shutil
from string import ascii_uppercase
from random import choice

from trails import wizards


def generatePackageInit(packageName,
                        packageDir,
                        bIncludeClassNode=True,
                        bIncludeFooLib=True,
                        bIncludeUINodeFactory=True,
                        bIncludePin=True,
                        bIncludeUIPinFactory=True,
                        bIncludeTool=True,
                        bIncludeExporter=True,
                        bIncludePinInputWidgetFactory=True,
                        bIncludePrefsWindget=False):
    result = "PACKAGE_NAME = '{0}'\n\n".format(packageName)
    result += "from collections import OrderedDict\n"
    result += "from trails.ui.ui_interfaces import IPackage\n\n"

    if bIncludePin:
        result += "# Pins\n"
        result += "from trails.packages.{0}.pins.demo_pin import DemoPin\n\n".format(packageDir)

    if bIncludeFooLib:
        result += "# Function based nodes\n"
        result += "from trails.packages.{0}.function_libraries.demo_lib import DemoLib\n\n".format(packageDir)

    if bIncludeClassNode:
        result += "# Class based nodes\n"
        result += "from trails.packages.{0}.nodes.demo_node import DemoNode\n\n".format(packageDir)

    if bIncludeTool:
        result += "# Tools\n"
        result += "from trails.packages.{0}.tools.demo_shelf_tool import DemoShelfTool\n".format(packageDir)
        result += "from trails.packages.{0}.tools.demo_dock_tool import DemoDockTool\n\n".format(packageDir)

    if bIncludeExporter:
        result += "# Exporters\n"
        result += "from trails.packages.{0}.exporters.demo_exporter import DemoExporter\n\n".format(packageDir)

    result += "# Factories\n"
    if bIncludeUIPinFactory:
        result += "from trails.packages.{0}.factories.ui_pin_factory import createUIPin\n".format(packageDir)

    if bIncludeUINodeFactory:
        result += "from trails.packages.{0}.factories.ui_node_factory import createUINode\n".format(packageDir)

    if bIncludePinInputWidgetFactory:
        result += "from trails.packages.{0}.factories.pin_input_widget_factory import getInputWidget\n".format(packageDir)

    if bIncludePrefsWindget:
        result += "# Prefs widgets\n"
        result += "from trails.packages.{0}.prefs_widgets.demo_prefs import DemoPrefs\n".format(packageDir)

    result += "\n"

    # TODO: Optimize this. Do not declare containers if feature not enabled
    result += "_FOO_LIBS = {}\n"
    result += "_NODES = {}\n"
    result += "_PINS = {}\n"
    result += "_TOOLS = OrderedDict()\n"
    result += "_PREFS_WIDGETS = OrderedDict()\n"
    result += "_EXPORTERS = OrderedDict()\n\n"

    if bIncludeFooLib:
        result += """_FOO_LIBS[DemoLib.__name__] = DemoLib(PACKAGE_NAME)\n\n"""

    if bIncludeClassNode:
        result += """_NODES[DemoNode.__name__] = DemoNode\n\n"""

    if bIncludePin:
        result += """_PINS[DemoPin.__name__] = DemoPin\n\n"""

    if bIncludeTool:
        result += """_TOOLS[DemoShelfTool.__name__] = DemoShelfTool\n"""
        result += """_TOOLS[DemoDockTool.__name__] = DemoDockTool\n\n"""

    if bIncludeExporter:
        result += """_EXPORTERS[DemoExporter.__name__] = DemoExporter\n\n"""

    if bIncludePrefsWindget:
        result += """_PREFS_WIDGETS["Demo"] = DemoPrefs\n\n"""

    result += "\nclass {0}(IPackage):\n\tdef __init__(self):\n\t\tsuper({0}, self).__init__()\n\n".format(packageName)
    result += """\t@staticmethod\n\tdef GetExporters():\n\t\treturn _EXPORTERS\n\n"""
    result += """\t@staticmethod\n\tdef GetFunctionLibraries():\n\t\treturn _FOO_LIBS\n\n"""
    result += """\t@staticmethod\n\tdef GetNodeClasses():\n\t\treturn _NODES\n\n"""
    result += """\t@staticmethod\n\tdef GetPinClasses():\n\t\treturn _PINS\n\n"""
    result += """\t@staticmethod\n\tdef GetToolClasses():\n\t\treturn _TOOLS\n\n"""

    if bIncludeUIPinFactory:
        result += """\t@staticmethod\n\tdef UIPinsFactory():\n\t\treturn createUIPin\n\n"""

    if bIncludeUINodeFactory:
        result += """\t@staticmethod\n\tdef UINodesFactory():\n\t\treturn createUINode\n\n"""

    if bIncludePinInputWidgetFactory:
        result += """\t@staticmethod\n\tdef PinsInputWidgetFactory():\n\t\treturn getInputWidget\n\n"""

    if bIncludePrefsWindget:
        result += """\t@staticmethod\n\tdef prefs_widgets():\n\t\treturn _PREFS_WIDGETS\n\n"""

    return result


def generatePackage(packageName,
                    packageDir,
                    newPackageRoot,
                    bIncludeClassNode=True,
                    bIncludeFooLib=True,
                    bIncludeUINodeFactory=True,
                    bIncludePin=True,
                    bIncludeUIPinFactory=True,
                    bIncludeTool=True,
                    bIncludeExporter=True,
                    bIncludePinInputWidgetFactory=True,
                    bIncludePrefsWindget=False):
    wizardsRoot = wizards.__path__[0]
    templatesRoot = os.path.join(wizardsRoot, "templates")
    packageTemplateDirPath = os.path.join(templatesRoot, "package_template")
    newPackagePath = os.path.join(newPackageRoot, packageDir)

    if os.path.exists(newPackagePath):
        shutil.rmtree(newPackagePath)
    shutil.copytree(packageTemplateDirPath, newPackagePath)

    for path, dirs, files in os.walk(newPackagePath):
        for newFileName in files:
            pyFileName = newFileName.replace(".txt", ".py")
            pyFilePath = os.path.join(path, pyFileName)
            txtFilePath = os.path.join(path, newFileName)
            with open(txtFilePath, "r") as f:
                txtContent = f.read()
                pyContent = txtContent.replace("@PACKAGE_NAME", packageName)
                pyContent = pyContent.replace("@RAND", "".join([choice(ascii_uppercase) for i in range(5)]))
                with open(pyFilePath, "w") as pf:
                    pf.write(pyContent)
            os.remove(txtFilePath)

    moduleInitFilePath = os.path.join(newPackagePath, "__init__.py")
    with open(moduleInitFilePath, "w") as f:
        f.write(generatePackageInit(packageName, packageDir,
                                    bIncludeClassNode=bIncludeClassNode,
                                    bIncludeFooLib=bIncludeFooLib,
                                    bIncludeUINodeFactory=bIncludeUINodeFactory,
                                    bIncludePin=bIncludePin,
                                    bIncludeUIPinFactory=bIncludeUIPinFactory,
                                    bIncludeTool=bIncludeTool,
                                    bIncludeExporter=bIncludeExporter,
                                    bIncludePinInputWidgetFactory=bIncludePinInputWidgetFactory,
                                    bIncludePrefsWindget=bIncludePrefsWindget))

    # remove unneeded directories
    for path, dirs, files in os.walk(newPackagePath):
        dirName = os.path.basename(path)
        if dirName == "nodes" and not bIncludeClassNode:
            shutil.rmtree(path)
        if dirName == "function_libraries" and not bIncludeFooLib:
            shutil.rmtree(path)
        if dirName == "pins" and not bIncludePin:
            shutil.rmtree(path)
        if dirName == "tools" and not bIncludeTool:
            shutil.rmtree(path)
        if dirName == "exporters" and not bIncludeExporter:
            shutil.rmtree(path)
        if dirName == "prefs_widgets" and not bIncludePrefsWindget:
            shutil.rmtree(path)
        if dirName == "factories":
            removedFactoriesCount = 0

            if not bIncludeUINodeFactory:
                os.remove(os.path.join(path, "ui_node_factory.py"))
                removedFactoriesCount += 1
            if not bIncludeUIPinFactory:
                os.remove(os.path.join(path, "ui_pin_factory.py"))
                removedFactoriesCount += 1
            if not bIncludePinInputWidgetFactory:
                os.remove(os.path.join(path, "pin_input_widget_factory.py"))
                removedFactoriesCount += 1

            if removedFactoriesCount == 3:
                shutil.rmtree(path)

        if dirName == "ui":
            removedUIClasses = 0

            if not bIncludePin or not bIncludeUIPinFactory:
                os.remove(os.path.join(path, "ui_demo_pin.py"))
                removedUIClasses += 1

            if not bIncludeClassNode or not bIncludeUINodeFactory:
                os.remove(os.path.join(path, "ui_demo_node.py"))
                removedUIClasses += 1

            if removedUIClasses == 2:
                shutil.rmtree(path)
