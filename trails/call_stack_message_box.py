"""
Copyright (C) 2015-2019  ?? Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Cristina Gabriela Gavrila

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import os
import sys
import traceback

from Qt.QtWidgets import QMessageBox, QTextEdit

cwd = os.getcwd()


def makePathRelative(path):
    if not os.path.isabs(path):
        return path

    prefix = os.path.commonpath([path, cwd])
    return path[len(prefix) + 1:]


def parseTraceback(e, value, tb):
    exception = "{}: {}".format(e.__name__, str(value))
    tracebackList = "\n".join(
        [
            f'File "{makePathRelative(file)}", line {line}, in {function}' + (f'\n\t{text}' if text else '')
            for file, line, function, text in traceback.extract_tb(tb)[::-1]
        ]
    )

    return "\n".join(
        ["Traceback (most recent call first):", tracebackList, exception]
    )


def showCallStackMessageBox(e: Exception, title: str, text: str = None):
    msgBox = QMessageBox(None)
    msgBox.setIcon(QMessageBox.Critical)
    msgBox.setWindowTitle(title)
    msgBox.setText(str(e) if text is None else text)
    msgBox.setDetailedText(parseTraceback(*sys.exc_info()))

    # show details by default
    for button in msgBox.buttons():
        if msgBox.buttonRole(button) == QMessageBox.ActionRole:
            button.click()
            break

    # adapt size to better show the callstack
    textBoxes = msgBox.findChildren(QTextEdit)
    if textBoxes:
        textBoxes[0].setFixedSize(750, 250)

    msgBox.exec()
