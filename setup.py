"""
Copyright (C) 2019  Lorenz Lechner

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from setuptools import setup, find_packages
import sys
import os

sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), "PyFlow", "Core"))
from version import Version

setup(
    name="PyFlow",
    version=str(Version()),
    packages=find_packages(),
    entry_points={
        'console_scripts': ['pyflow = PyFlow.Scripts:main']
    },
    include_package_data=True,
    author="Ilgar Lunin, Pedro Cabrera",
    author_email="wonderworks.software@gmail.com",
    description="A general purpose runtime extendable python qt node editor.",
    keywords="visual programming framework",
    url="https://wonderworks-software.github.io/PyFlow",   # project home page
    project_urls={
        "Bug Tracker": "https://github.com/wonderworks-software/PyFlow/issues",
        "Documentation": "https://pyflow.readthedocs.io",
        "Source Code": "https://github.com/wonderworks-software/PyFlow",
    },
    classifiers=[
        'License :: Appache-2.0'
    ],
    install_requires=["enum ; python_version<'3.4'", "Qt.py", "blinker", "nine", "docutils"],
    extra_requires=["PySide2"]
)
