#! /usr/bin/env python3
"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2018  Mel Massadian
Copyright (C) 2019  Lorenz Lechner
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import sys
import signal
import argparse
import pathlib
import os.path

from Qt.QtCore import Qt
from Qt.QtWidgets import QApplication

from trails.app import Trails
from trails.core.version import Version
from trails.config_manager import (
    PreferencesManager, ShortcutsManager, AppStateManager, PLATFORMPATHS)


class SignalHandler:
    def __init__(self, instance):
        self.instance = instance

    def signal(self, signal, frame):
        self.instance.close()


class CustomHelpFormatter(argparse.HelpFormatter):
    """An `argparse.HelpFormatter` which honors newline characters '\n'."""

    def _split_lines(self, text, width):
        text = self._whitespace_matcher.sub(' ', text).strip()
        # The textwrap module is used only for formatting help.
        # Delay its import for speeding up the common usage of argparse.
        import textwrap
        r = []
        for t in text.split('\\n'):
            r.extend(
                textwrap.wrap(
                    t.strip().replace('\\t', '\t'),
                    width=width,
                    expand_tabs=True, tabsize=2))
        return r

    def _fill_text(self, text, width, indent):
        text = self._whitespace_matcher.sub(' ', text).strip()
        import textwrap
        return '\n'.join([
            textwrap.fill(
                t.strip().replace('\\t', '\t'),
                width=width,
                initial_indent=indent, subsequent_indent=indent,
                expand_tabs=True, tabsize=2)
            for t in text.split('\\n')])

class PathType:
    """Factory for creating `pathlib.Path` objects.

    Keyword Arguments
    -----------------
    mode : 'read'|'write'|'overwrite', optional
        A string indicating, if the file is for reading (file must exist),
        writing (fiile must not exist) or overwriting.
        The default is 'read'.

    Raises
    ------
    ValueError
        Raised, if a wrong mode parameter is supplied.
    argparse.ArgumentTypeError
        Raised, if the file does not exist for reading or does exist for
        writing.

    """

    def __init__(self, mode='read'):
        if mode not in ('read', 'write', 'overwrite'):
            raise ValueError(
                f"mode must be 'read', 'write' or 'overwrite'; got: '{mode}'")
        self._mode = mode

    def __call__(self, string):
        if string == '-':
            # `sys.stdin` or `sys.stdout`
            if self._mode == 'read':
                return sys.stdin
            else:
                return sys.stdout

        path = pathlib.Path(string)
        if self._mode == 'read':
            if path.is_file():
                return path
            else:
                raise argparse.ArgumentTypeError(
                    f"file '{path}' does not exist (cannot be read)")
        elif self._mode == 'write':
            if path.is_file():
                raise argparse.ArgumentTypeError(
                    f"file '{path}' does exist (cannot be written)")
            else:
                return path
        else:
            return path


def parse_args(argv):
    """Parse arguments.

    Parameters
    ----------
    argv : list
        List with arguments, similar to `sys.argv`, but without the program name.

    Returns
    -------
    args : argparse.Namespace
        The parsed arguments (without the files to be opened).
    files : list
        The list with the files to open.

    """
    # Default values from `PreferencesManager()`
    DEFAULTS = PreferencesManager().DEFAULTS
    CONFDIRS = [
        *PLATFORMPATHS.site_config_paths,
        PLATFORMPATHS.user_config_path]
    CONFFILES = [
        str(p.joinpath('CONFIGFILE'))
        for p in CONFDIRS]
    CONFFILES.append('./CONFIGFILE')
    # String with a list of default configuration files
    CONFFILESSTRING = r'\n\t- '.join(CONFFILES)

    parser = argparse.ArgumentParser(
        description="A general purpose visual scripting framework for Python.",
        epilog="Homepage: https://codeberg.org/Trails/Trails/",
        formatter_class=CustomHelpFormatter)

    # Optional arguments

    parser.add_argument(
        '-f', '--file',
        type=PathType(mode='read'),
        dest='graphfile',
        metavar='FILE',
        help=r'''
            the graph file(s) to be opened
            (commonly the file ending is ".pygraph")
            ''')

    parser.add_argument(
        '-v', '--version',
        action='version',
        version=f'Trails {Version()}')

    parser.add_argument(
        '-c', '--configuration',
        type=PathType(mode='read'),
        dest='configfile',
        metavar='CONFIGFILE',
        help='''
            main configuration file to be used instead of the standard files
            ''')

    parser.add_argument(
        '--shortcuts',
        type=PathType(mode='read'),
        dest='shortcutsfile',
        metavar='CONFIGFILE',
        help='''
            file with input shortcuts to be used instead of the standard files
            ''')

    parser.add_argument(
        '--state',
        type=PathType(mode='read'),
        dest='statefile',
        metavar='CONFIGFILE',
        help='''
            file with saved program's state to be used instead of the standard
            files
            ''')

    # General settings

    group = parser.add_argument_group('general settings')

    group.add_argument(
        '-p', '--package',
        action='append',
        dest='General.ExtraPackageDirs',
        metavar='PACKAGEDIR',
        help=rf'''
            additional directories, where to look for packages\n
            Default config value:\n
                \t%(dest)s = {', '.join(DEFAULTS['General']['ExtraPackageDirs'])}
            ''')

    group.add_argument(
        '-e', '--editor',
        dest='General.EditorCmd',
        metavar='"EDITOR @FILE"',
        help=rf'''
            editor to be used to edit files (must be enclosed in quotes);
            "@FILE" will be replaced with the file to be edited\n
            Default config value:\n
                \t%(dest)s = {DEFAULTS['General']['EditorCmd']}
            ''')

    group.add_argument(
        '-u', '--undo-levels',
        type=int,
        dest='General.HistoryDepth',
        metavar='LEVELS',
        help=rf'''
            number of actions to be saved in the undo history\n
            Default config value:\n
                \t%(dest)s = {DEFAULTS['General']['HistoryDepth']}
            ''')

    group_m = group.add_mutually_exclusive_group()
    group_m.add_argument(
        '-a', '--auto-zoom',
        nargs='?',
        type=float,
        const=True, default=None,
        dest='autozoom',
        metavar='ZOOMFACTOR',
        help=rf'''
            automatically zoom according to the dpi settings of the screen\n
            Default config values:\n
                \tGeneral.AutoZoom = {DEFAULTS['General']['AutoZoom']}\n
                \tGeneral.AdjAutoZoom = {DEFAULTS['General']['AdjAutoZoom']}
            ''')

    group_m.add_argument(
        '-z', '--zoom',
        type=float,
        dest='General.InitialZoom',
        metavar='ZOOMFACTOR',
        help=rf'''
            the initial zoom factor\n
            Default config values:\n
                \tGeneral.AutoZoom = {DEFAULTS['General']['AutoZoom']}\n
                \t%(dest)s = {DEFAULTS['General']['InitialZoom']}
            ''')

    group.add_argument(
        '-r', '--redirect',
        default=None,
        action='store_true',
        dest='General.RedirectOutput',
        help=rf'''
            the log entries are redirected to the terminal's output\n
            Default config value:\n
                \t%(dest)s = {DEFAULTS['General']['RedirectOutput']}
            ''')

    # User interface settings

    group = parser.add_argument_group('user interface')

    group.add_argument(
        '-t', '--theme',
        dest='UI.Theme',
        help=rf'''
            the theme to be used for the user interface\n
            Default config value:\n
                \tUI.Theme  = {DEFAULTS['UI']['Theme']}
            ''')

    # Graph area settings

    group = parser.add_argument_group('graph area')

    group.add_argument(
        '-g', '--graph-theme',
        dest='Graph.Theme',
        help=rf'''
            the theme to be used for the graph area\n
            Default config value:\n
                \tGraph.Theme = {DEFAULTS['Graph']['Theme']}
            ''')

    # Configuration files

    parser.add_argument_group(
        'configuration files CONFIGFILE',
        description=rf'''
            \n
            Trails uses three configuration files:\n
            \t- 'trails.yaml': Main configuration file\n
            \t- 'shortcuts.yaml': Keyboard/mouse shortcuts\n
            \t- 'state.yaml': The window's state, saved when the program is
                closed\n
            \n
            These configuration files are loaded from these typical
            locations:\n
            \t- {CONFFILESSTRING}\n
            The files are merged and the settings from files further down in
            this list are taking precedence over earlier ones. Command line
            parameters override them all.\n
            \n
            If any of the optional configuration options
            '-c|--config CONFIGFILE', '--shortcuts CONFIGFILE' or
            '--state CONFIGFILE' is given, only the specified CONFIGFILE is
            used.\n
            \n
            All the configuration files are in the YAML format. The keys and
            default values of the main configuration file 'trails.yaml' are
            given above, e.g. the option 'General.HistoryDepths' for the number
            of undo levels ('-u|--undo-levels LEVELS') would translate to this
            entry in the 'trails.yaml' file:\n
            \tGeneral:\n
            \t\tHistoryDepths: 50\n
            ''')

    args = parser.parse_args(argv)

    # We have to work with `getattr` and `setattr`, because `Namespace` cannot
    # handle attributes with dots '.'.
    if args.autozoom is not None:
        setattr(args, 'General.AutoZoom', True)
        if args.autozoom is True:
            setattr(args, 'General.AdjAutoZoom', None)
        else:
            setattr(args, 'General.AdjAutoZoom', args.autozoom)
    elif getattr(args, 'General.InitialZoom') is not None:
        setattr(args, 'General.AutoZoom', False)
        setattr(args, 'General.AdjAutoZoom', None)
    else:
        setattr(args, 'General.AutoZoom', None)
        setattr(args, 'General.AdjAutoZoom', None)
    del args.autozoom

    # Separate out the files to be opened
    graphfile = args.graphfile
    del args.graphfile

    return args, graphfile


def configure(args):
    """Update the `PreferencesManager()` with the parsed arguments.

    Parameters
    ----------
    args : argparse.Namespace
        The parsed arguments. The keys must conform to valid key value for
        `upsilonconf.Configuration` and 'configfile'.

    """


    settings = PreferencesManager()

    # Load the configuration file, if given
    if args.configfile:
        settings.load(fname=args.configfile)
    del args.configfile

    # Other arguments are configuration values
    for key, value in vars(args).items():
        if isinstance(value, list):
            # Append list to alrady existing list
            settings[key].extend(value)
        elif value is not None:
            # Set to the new value
            settings.overwrite(key, value)


def configure_shortcuts(args):
    """Replaces shortcuts in `ShortcutsManager()` with values from `args.shortcutsfile`.

    Parameters
    ----------
    args : argparse.Namespace
        The parsed arguments.

    """

    shortcuts = ShortcutsManager()

    # Load the shortcuts file, if given
    if args.shortcutsfile:
        shortcuts.load(fname=args.shortcutsfile)
    del args.shortcutsfile


def configure_state(args):
    """Replaces states in `AppStateManager()` with values from `args.statefile`.

    Parameters
    ----------
    args : argparse.Namespace
        The parsed arguments.

    """

    state = AppStateManager()

    # Load the shortcuts file, if given
    if args.statefile:
        state.load(fname=args.statefile)
    del args.statefile


def main():
    # Enable general High DPI scaling
    QApplication.setAttribute(Qt.AA_EnableHighDpiScaling)
    # Enable High DPI icons
    QApplication.setAttribute(Qt.AA_UseHighDpiPixmaps, True)

    # Initialize the application
    app = QApplication(sys.argv)

    # Get unconsumed arguments and parse them
    argv = app.arguments()
    args, graphfile = parse_args(argv[1:])

    # Update the preferences system
    configure(args)
    configure_shortcuts(args)
    configure_state(args)

    instance = Trails.instance(software="standalone")
    if instance is not None:
        # Act on SIGINT (Ctrl-C)
        signal.signal(signal.SIGINT, SignalHandler(instance).signal)

        app.setActiveWindow(instance)
        instance.show()

        if graphfile:
            instance.loadFromFile(graphfile)

        try:
            sys.exit(app.exec_())
        except Exception as e:
            print(e)


if __name__ == '__main__':
    main()
